# WfLibrary
This is the library that allows to modify WordFast Files (TXLF and TXML).
The library currently does not support TMs.

## Installation
To install the library clone it. And then run

>pip install .

Inside the folder
As well, you can use one of the wheels.

## Contribution
Before being able to collaborate with the project you need to clone the project, and create a new branch to apply your changes.
Once the changes are done. Create a "Pull request" in the project and it will allow to the mantainer to know that there is
a request of a modification of the source code.

## Usage
Once the library is installed. You can import the modules into your project and start using it.

```python
# Import the library
import wf_library # Imports all modules
# Or
from wf_library import Wf # Import the module to handle WordFast files (TXLF/TXML)

```

Once the library is imported you can start using it. Let's see a few examples of the usage of the module `Wf`

```python
from wf_libray import Wf

# Load the WordFast file
wf = Wf('Path to the file as string')

# Let's find a segment containing a specific text in the source:
seg = wf.find('Power of Greyskull', case_sensitive=False) # It returns a Segment object that contains that text
seg.target.text = 'Por el poder de Greyskull' # Set the value of the target.
# Once modified the file, it needs to be saved
wf.save()
```

The object Wf has some children classes that would be `Paragraph` that represents a Paragraph/Group in the TXML/TXLF files.
The `Segment` object represents the `trans-unit` or `segment` elements in the TXLF and TXML files respectivelly. The `Segment` object is child of `Paragraph`.
And the last child, that is child of `Segment` is `SegmentContent` that represents the source/target elements.

There is another module that is used to generate ExcelConfig files. To use it you might need to import 2 classes

```python
from wf_library import ExcelConfig, Sheet

config_file = ExcelConfig() # Initialize an empty ExcelConfig object with some of the parameters set as default.
# Define a Sheet to be added to the config
sheet = Sheet()
sheet.set_source_column_range('0:3;5-8') # It adds a columns range with the same syntax as is used in the XML Excel Config file
sheet.name = 'Sheet1'
conf_file.add_sheet(sheet) # Once defined the configuration for the sheet, can be added to the ExcelConfig object with this method
```
