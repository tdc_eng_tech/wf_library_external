__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from lxml import etree as et


class Grade:
    """
    Grade of a review
    """
    def __init__(self, xml_grade: et.Element, wf_version: str, parent: object=None):
        self._xml = xml_grade
        self.wf_version = wf_version
        self._parent = parent
        self._id = None
        self._grade = None
        self._score = None
        self.__initialize()
    
    def __repr__(self):
        return f'<Grade {self.id}>'
    
    def __initialize(self) -> None:
        if self._xml is not None:
            self._grade = self._xml.get('grade')
            self._id = self._xml.get('gId')
            self._score = self._xml.get('score')
    
    def delete(self):
        """
        """
        self._parent.grades.remove(self)
        self._xml.getparent().remove(self._xml)
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def id(self):
        """
        """
        if not self._id:
            self._id = self._xml.get('gId')
        return self._id
    
    @property
    def grade(self):
        """
        """
        if not self._grade:
            self._grade = self._xml.get('grade')
        return self._grade
    
    @grade.setter
    def grade(self,  value: str):
        if isinstance(value, str):
            self._grade = value
            self._xml.set('grade', value)
        else:
            raise TypeError(f'The score must be "str" and it is {type(value)}')
            
    
    @property
    def score(self):
        """
        """
        if not self._score:
            self._score = self._xml.get('score')
        return self._score

    @score.setter
    def score(self,  value: str):
        if isinstance(value, str):
            self._score = value
            self._xml.set('score', value)
        else:
            raise TypeError(f'The score must be "str" and it is {type(value)}')
