__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from typing import Literal


txlf_nsmap = {'gs4tr': 'http://www.gs4tr.org/schema/xliff-ext', 
                        'Default': 'urn:oasis:names:tc:xliff:document:1.2'
                        }

                        
CONTENTTYPE = Literal['source', 'target']
