__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from lxml import etree as et
from pathlib import Path
from os import getlogin
from datetime import datetime
import re
from copy import deepcopy
import random

from .const import txlf_nsmap
from .segmentcontent import SegmentContent
from .review_info import ReviewInfo
from .revision import Revision



class Segment:
    """
    Represent the segment part of the bilingual file. 
    There is stored the source and the target content.
    This class hass not be directly instantiated. Instead use the Paragraph.segments.
    
    Example:
        wf_file = Wf('c:\file\wf_file.txlf')
        for paragraph in wf_file.paragraphs:
            print(len(paragraph.segements)) # Total of segments of the paragraph
    
    Attributes:
        source (dict): Contains the information of the source. It has the following keys:
            text (str): It shows the full text, indicating the position of the tags as {tag}.
            text_only (str): It removes all tags only showing the actual content.
            xml (Element): It shows the whole xml code of the source element.
            xml_string (str): The xml in string format.
        target (dict): Target information. Has the same structure as source.
        paragraph (Paragraph): reference to the parent Paragraph.
        wf (Wf): reference to Wf object.
        terms (list): List of terms that has been looked for specific purposes.
    """
    def __init__(self, xml_segment, wf_version, hidden: bool=False, par_position: int=1,
                        parent: object=None, paragraph: object=None, wf: object=None, seg_number: int=1):
        """
        Initialization of the class Segment.
        
        Example:
            segment = Segment(xml_segment, 'txlf')
            
        Arguments:
            xml_segment (Element): <segment> XML element of the Wf file.
            wf_version (str): Version of Wf. Can be txlf or txml.
            hidden (bool): Indicates if the segment is in translate status or not
            par_position (int): Indicates the position of the segment in the paragraph
        """

        self.xml_seg = xml_segment
        self.wf_version = wf_version
        
        self.__id = ''
        self.terms = []
        self._add_source()
        self._add_target()
        self.__number = seg_number
        self.__notes = []
        self.__maxlen = None
        self.__locked = False
        self.__found_in_preview = False
        self.__translate = True
        self.__user_attributes = dict()
        self.__attributes = {}
        self._paragraph = paragraph
        self._wf = wf
        self._revisions = []
        self._xml_revisions = None
        self._hidden = hidden
        self.xml_comment = None
        self._parent = parent
        self._paragraph_position = par_position
        self._reviews = []
        self.load_review_info()
        self.load_revisions()
    
    def __repr__(self):
        return f'<Segment {self.id}>'
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def wf(self):
        return self._wf
    
    @property
    def xml(self):
        """
        XML of the segment
        """
        return self.xml_seg
    
    @xml.setter
    def xml(self, value: et.Element):
        if isinstance(value, et.Element):
            self.xml_seg = value
        else:
            raise TypeError('It should be an etree.Element and it is another type.')
    
    @property
    def previous(self):
        """
        Get the previous segment
        """
        if self.paragraph.segments.index(self) > 0:
            return self.paragraph.segments[self.paragraph.segments.index(self) - 1]
        else:
            raise StopIteration('It was the first segment, there are no more.')
    
    @property
    def next(self):
        """
        Get the next segment
        """
        if self.paragraph.segments.index(self) < len(self.paragraph.segments) - 1:
            return self.paragraph.segments[self.paragraph.segments.index(self) + 1]
        else:
            raise StopIteration('It was the last segment, there are no more.')
    
    @property
    def seg_number(self):
        """
        Number of the segment in the file. It is independent to the id of the segment
        in a paragraph. It is the value that would be shown in the visual tool like 
        WordFast, TSO or TSR.
        """
        return self.__number
    
    @seg_number.setter
    def seg_number(self, value):
        self.__number = value
    
    @property
    def paragraph_position(self):
        """
        Return the position of the segment in the paragraph.
        """
        return self._paragraph_position
    
    @property
    def id(self):
        """
        Identifier/number of the segment in the paragraph.
        """
        if self.wf_version == 'txlf':
            
            self.__id = self.xml_seg.attrib['id']
        else:
            self.__id = self.xml_seg.attrib['segmentId']
        return self.__id
    
    @property
    def attributes(self):
        """
        Get all the attributes of the paragraph.
        It will return the raw attributes and not formatted.
        """
        if not self.__attributes:
            for attribute, value in self.xml_seg.attrib.items():
                self.__attributes[attribute] = value
        return self.__attributes
    
    @property
    def revisions(self):
        return self._revisions
    
    @property
    def paragraph(self):
        return self._paragraph
    
    @property
    def revision_notes(self):
        """
        Read the revision notes of the segment
        """
        if self.wf_version == 'txlf':
            xml_notes = self.xml_seg.xpath('gs4tr:review/gs4tr:note',  namespaces=txlf_nsmap)
        if xml_notes:
            return [note.text for note in xml_notes]
    
    @property
    def notes(self):
        """
        Get the notes. Only 1 note can be set.
        TODO:
            Change the property to read only and change the setter to
                         a full method allowing to add more notes.
        """
        if self.wf.wf_file:
            if self.wf_version == 'txlf':
                self.xml_notes = self.xml_seg.xpath('gs4tr:note', namespaces=txlf_nsmap)
                
            else:
                self.xml_notes = self.xml_seg.xpath('./comments/comment')
            if self.xml_notes:
                self.__notes = [xml_notes.text for xml_notes in self.xml_notes]
            else: self.__notes = []
        return self.__notes
    
    def add_review(self,
                            user: str,
                            timestamp: str=datetime.utcnow().strftime("%Y%m%dT%H%M%SZ"),
                            tool_name: str='TSR', 
                            tool_version: str='6.0.3', 
                            phase_name: str='Review-1'
                            ):
        """
        Add a review object
        Arguments:
            user (str): The username to be added, matching with the from
            timestamp (str): Date of creation of the Review
        """
        xml_review = et.SubElement(self.xml, '{http://www.gs4tr.org/schema/xliff-ext}review')
        xml_review.set('from', user)
        xml_review.set('timestamp', timestamp)
        xml_review.set('tool-name', tool_name)
        xml_review.set('tool-version', tool_version)
        xml_review.set('phase-name', phase_name)
        review = ReviewInfo(xml_review, self.wf_version, self)
        self.reviews.append(review)
    
    def delete_review(self, review: ReviewInfo):
        """
        Delete a specific review
        """
        rev = self.reviews.pop(self.reviews.index(review))
        rev.xml.getparent().remove(rev.xml)
    
    def delete_reviews(self, children=False):
        """
        Delete all the reviews of the segment.
        
        Arguments:
            children (bool): Delete the reviews as well for any children elements like Revisions.
        """
        for rev in self.reviews:
            self.delete_review(rev)
        if children:
            for revision in self.revisions:
                revision.delete_reviews()
    
    def add_note(self,
                         value: str,
                         num_note: int=0,
                         username='',
                         date_creation: str=datetime.utcnow().strftime("%Y%m%dT%H%M%SZ"),
                         type: str='translation') -> None:
        """
        Adds note allowing more flexibility than the notes setter making it deprecated 
        and obsolete and to remove in next versions.
        Arguments:
            value (str): Adds the text to the note.
            num_note (str): Number of the note.
            username (str): User name that created the note.
            date_creation (str): Date string of when the note was created
            type (str): Identifies what type of note it was. For example, translation, proofreading...
        Return:
            None
        """
        if value:
            self.__notes.append(value)
            if not username:
                try:
                    username = getlogin()
                except OSError as e:
                    if 'errno 6' in str(e):
                        username = 'tpt_generic'
            if self.wf.wf_file:
                user_name = username
                if self.wf_version == 'txlf':
                    if num_note == 0:
                        if not self.notes:
                            note_num = num_note
                        else:
                            note_num = len(self.notes)
                    else:
                        note_num = num_note
                    creation_date = date_creation
                    segment_note_type = type 
                    note_sub_elem = et.SubElement(self.xml_seg,
                     '{http://www.gs4tr.org/schema/xliff-ext}note')
                    note_sub_elem.attrib['snid'] = str(note_num)
                    note_sub_elem.attrib['from'] = user_name
                    note_sub_elem.attrib['timestamp'] = creation_date
                    note_sub_elem.attrib['segment-note-type'] = segment_note_type
                else:
                    creation_date = date_creation
                    if type == 'translation':
                       segment_note_type = 'text'
                    else:
                       segment_note_type = type 
                    if self.notes is None:
                        notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    else:
                        try:
                            notes_sub_elem = self.xml_seg.xpath('./comments')[0]
                        except IndexError:
                            notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    note_sub_elem = et.SubElement(notes_sub_elem, 'comment')
                    note_sub_elem.attrib['creationid'] = user_name
                    note_sub_elem.attrib['creationdate'] = creation_date
                    note_sub_elem.attrib['type'] = segment_note_type
                note_sub_elem.text = value
   
    @notes.setter
    def notes(self, value):
        if value:
            self.__notes.append(value)
            
            if self.wf.wf_file:
                user_name = getlogin()
                
                if self.wf_version == 'txlf':
                    if len(self.notes) == 0:
                        note_num = 0
                    else:
                        note_num = len(self.notes)
                    creation_date = datetime.utcnow().strftime("%Y%m%dT%H%M%SZ")
                    note_sub_elem = et.SubElement(self.xml_seg,
                     '{http://www.gs4tr.org/schema/xliff-ext}note')
                    note_sub_elem.attrib['snid'] = str(note_num)
                    note_sub_elem.attrib['from'] = user_name
                    note_sub_elem.attrib['timestamp'] = creation_date
                    note_sub_elem.attrib['segment-note-type'] = 'translation'
                    
                else:
                    creation_date = datetime.utcnow().strftime("%Y%m%dT%H%M%SZ")
                    if self.notes is None:
                        notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    else:
                        try:
                            notes_sub_elem = self.xml_seg.xpath('./comments')[0]
                        except IndexError:
                            notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    note_sub_elem = et.SubElement(notes_sub_elem, 'comment')
                    note_sub_elem.attrib['creationid'] = user_name
                    note_sub_elem.attrib['creationdate'] = creation_date
                    note_sub_elem.attrib['type'] = 'text'
                #print('The value to be inserted in the note: ', value)
                note_sub_elem.text = value
                #print('The text of the note: ', note_sub_elem.text)
                
        elif not value or value == '':
            self.__notes = []
            if self.wf.wf_file:
                if self.wf_version == 'txlf':
                    try:
                        for note in self.xml_seg.findall('gs4tr:note', namespaces=txlf_nsmap):
                            self.xml_seg.remove(note)
                    except:
                        pass
                else:
                    try:
                        self.xml_seg.remove(self.xml_seg.find('comments'))
                    except:
                        pass
                    
        #self.wf.update_root(et.ElementTree(self.wf.xml_tree.getroot())) 

    @property
    def maxlen(self):
        """
        Handles the maxlen of a paragraph. Get and set the maxlen of a paragraph.
        """
        if not self.__maxlen and Path(self.wf.wf_file).is_file():
            try:
                if self.wf_version == 'txlf':
                    self.__maxlen = self.xml_seg.attrib[
                             '{http://www.gs4tr.org/schema/xliff-ext}maxlen']
                else:
                    self.__maxlen = self.xml_seg.attrib['maxlen']
            except KeyError:
                self.__maxlen = None
        return self.__maxlen
    
    def add_user_attribute(self, key, value):
        """
        Add the user attributes to the segment.
        The user attributes for the old Wf version (TXML) are added with prefix satt_ and for the new format
        are added as a value for the attribute user-attributes.
        
        Example:
        TXML
        satt_note="Something"
        
        TXLF:
        user-attributes= "&lt;attr note=&quot;Something&quot;&gt;"
        
        Arguments:
            key: Name of the attribute to be added.
        """
        #pdb.set_trace()
        self.__user_attributes[key] = value      
        if self.wf.wf_file_path.is_file():                     
            if self.wf_version == 'txlf':
                try:
                    user_attributes = self.xml_seg.attrib[
                                '{http://www.gs4tr.org/schema/xliff-ext}user-attributes']                  
                    attrib_xml = et.fromstring(user_attributes)
                except KeyError:
                    attrib_xml = et.Element('attrib')
                attrib_xml.attrib[key] = value    
                self.xml_seg.attrib[
                    '{http://www.gs4tr.org/schema/xliff-ext}user-attributes'] = et.tostring(attrib_xml)
            else:
                self.xml_seg.attrib[key] = value
    
    @maxlen.setter
    def maxlen(self, value):
        self.__maxlen = value
        if Path(self.wf.wf_file).is_file():
            if self.wf_version == 'txlf':
                if value:
                    self.xml_seg.attrib[
                                 '{http://www.gs4tr.org/schema/xliff-ext}maxlen'
                                 ] = self.__maxlen
                else:
                    try:
                        self.xml_seg.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}maxlen')
                    except KeyError:
                        pass
            else:
                if value:
                    self.xml_seg.attrib['maxlen'] = self.__maxlen
                else:
                    try:
                        self.xml_seg.attrib.pop('maxlen')
                    except KeyError:
                        pass
    
    @property
    def found_in_preview(self):
        """
        Check if a segment is found in preview.
        It is a property mainly found in e-learning projects.
        """
        if self.wf_version == 'txlf':
            try:
                self.__found_in_preview = self.xml_seg.attrib.get(
                                '{http://www.gs4tr.org/schema/xliff-ext}foundInPreview')
            except KeyError:
                self.__found_in_preview = True
        return self.__found_in_preview

    @found_in_preview.setter
    def found_in_preview(self, value: bool):
        if self.wf_version == 'txlf':
            self.__found_in_preview = value
            if value:
                try:
                    self.xml_seg.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}foundInPreview'] = 'true'
                except KeyError:
                    pass
            else:
                self.xml_seg.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}foundInPreview'] = 'false'

    @property
    def locked(self):
        """
        Checks if the segment is locked. It is a property only valid for TXLF files.
        """
        if not self.__locked and self.wf_version == 'txlf':
            try:
                self.__locked = self.xml_seg.attrib.get(
                                        '{http://www.gs4tr.org/schema/xliff-ext}locked',
                                         False)
            except KeyError:
                self.__locked = False
        elif self.wf_version == 'txml':
            raise AttributeError('This property is only valid for wf_version=TXLF')
        return self.__locked

    @locked.setter
    def locked(self, value: bool):
        self.__locked = value
        if self.wf_version == 'txlf':
            if value:
                self.xml_seg.attrib[
                        '{http://www.gs4tr.org/schema/xliff-ext}locked'] = 'true'
            else:
                try:
                    self.xml_seg.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}locked')
                except KeyError:
                    pass
        else:
            raise AttributeError('This property is only valid for wf_version=TXLF')
    
    @property
    def wordcount(self) -> int:
        """
        Return the value of the attribute wordcount. It exists only if it has been leveraged.
        Status: Implemented for TXLF only.
        """
        wordcount = self.xml_seg.get('{http://www.gs4tr.org/schema/xliff-ext}wordcount')
        if wordcount:
            return int(wordcount)
        else:
            return None
    
    @property
    def hidden(self):
        """
        Check if the segment is for translation or not. It sets hidden attribute in TXLF files.
        This implementation is for future TXLF versions
        """
        if self.wf_version == 'txlf':
            try:
                self._hidden = False if self.xml_seg.attrib.get(
                    '{http://www.gs4tr.org/schema/xliff-ext}hidden', 'false') == 'false' else True
            except KeyError:
                self._hidden = True
        return self._hidden

    @hidden.setter
    def hidden(self, value: bool):
        self._hidden = value
        if self.wf_version == 'txlf' and (
          value is True or value is False):
            self.xml_seg.attrib[
                '{http://www.gs4tr.org/schema/xliff-ext}hidden'] = 'true' \
                    if value == True else 'false'
            if self.xml_seg.attrib.get(
                  '{http://www.gs4tr.org/schema/xliff-ext}hidden') == 'false':
                    self.xml_seg.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}hidden')

    def _add_source(self):
        """
        Internal method that sets the values of the source
        It is used to generate the source of the segment when Segment class
        is initialitzed.
        """
        #self.source = self.__add_tgt_src_values('source')
        self.source = SegmentContent('source', self.xml_seg, self.wf_version, self)
    
    def _add_target(self):
        """
        Internal method to set the values of target.
        It is used to generate the target of the segment when Segment class
        is initialitzed.
        """
        #self.target = self.__add_tgt_src_values('target')
        self.target = SegmentContent('target', self.xml_seg, self.wf_version, self)
    
    def find_terms(self, pattern_text, attribute, excluding_word=''):
        """
        Looks for a pattern and finds this text based on an attribute.
        Also it allows to exclude a word.
        """
        pattern = re.compile(pattern_text)
        if attribute:
            items_found =  pattern.findall(attribute)
            if excluding_word:
                self.terms = [term for term in items_found if term != excluding_word]
            else:
                self.terms = items_found
        return self.terms
    
    def flash_find_terms(self, keywords, attribute):
        """
        Looks for the specified keywords. It is a faster alternative to find_terms
        when the whole word is needed to searched.
        Arguments:
            keywords: String or tuple or list of strings containing the keywords to search
            attribute: Where has to be looked for the keyword.
        """
        from flashtext.keyword import KeyWordProcessor
        keyword_processor = KeyWordProcessor()
        if isinstance(keywords, str):
            keyword_processor.add_keyword(keywords)
        if isinstance(keywords, (list, tuple)):
            for keyword in keywords:
                keyword_processor.add_keyword(keyword)
        if attribute:
            self.terms = keyword_processor.extract_keywords(attribute)
        return self.terms
        
    def load_revisions(self):
        """
        Load the revisions that are present in the segment.
        """
        if self.xml_seg is not None:
            if self.wf_version == 'txlf':
                xml_revisions = self.xml_seg.xpath('Default:alt-trans', namespaces=txlf_nsmap)
                self._revisions = [Revision(xml_revision, self.wf_version, self
                              ) for xml_revision in xml_revisions]
            else:
                xml_revisions = self.xml_seg.xpath('revisions')
                self._revisions = [Revision(xml_revision, self.wf_version, self
                                  ) for xml_revision in xml_revisions]
    
    def load_review_info(self):
        """
        Read the items gs4tr:review from the segment.
        """
        if self.wf_version == 'txlf':
            if self.xml_seg is not None:
                xml_reviews = self.xml_seg.xpath('gs4tr:review',  namespaces=txlf_nsmap)
                for xml_review in xml_reviews:
                    self._reviews.append(ReviewInfo(xml_review, self.wf_version, self))
                    
    @property
    def reviews(self):
        """
        Return the reviews of the file
        """
        return self._reviews
    
    def delete_revisions(self):
        """
        Delete all the revisions of the segment
        """
        if self.wf_version == 'txlf':
            xml_revisions = self.xml_seg.findall('{urn:oasis:names:tc:xliff:document:1.2}alt-trans')
        else:
            xml_revisions = self.xml_seg.xpath('revisions')
        if xml_revisions:
            for revision in xml_revisions:
                self.xml_seg.remove(revision)
        if self.revisions:
            self.revisions = []
    
    def delete_revision(self, revision: Revision) -> Revision:
        """
        Delete a revision
        
        Arguments:
            revision (Revision): Revision to be deleted.
        
        Return:
            Revision : The deleted revision is returned.
        """
        rev = self.revisions.pop(self.revisions.index(revision))
        if rev:
            rev.xml.getparent().remove(rev.xml)
        return rev
    
    def add_revision(self, target: SegmentContent):
        """
        Add a target as a revision.
        Useful to control the changes done manually by the library.
        """
        try:
            idx = self.xml.index(self.revisions[0].xml)
        except IndexError:
            idx = len(self.xml)
        xml_rev = et.Element('{urn:oasis:names:tc:xliff:document:1.2}alt-trans')
        xml_rev.set('alttranstype', 'previous-version')
        for key, value in target.xml.attrib.items():
            if key in ('state', 'state-qualifier'):
                xml_rev.set(f'{{http://www.gs4tr.org/schema/xliff-ext}}{key}', value)
            else:
                xml_rev.set(key, value)
        xml_target = deepcopy(target.xml)
        for attr in xml_target.attrib:
            xml_target.attrib.pop(attr)
        xml_rev.append(xml_target)
        self.xml.insert(idx, xml_rev)
        rev = Revision(xml_rev, self.wf_version, self)
        self.revisions.insert(0, rev)
        
    def paste_full_target(self, target: SegmentContent):
        """
        Pastes the full target with the same attributes to the file.
        
        Arguments:
            target (SegmentContent): The segment from where need to be copied the target
        """
        if self.wf_version == 'txlf':
            tgt_elem = self.xml_seg.xpath(
                    'Default:{}'.format('target'), namespaces=txlf_nsmap)[0]
            src_elem = self.xml_seg.xpath(
                    'Default:{}'.format('source'), namespaces=txlf_nsmap)[0]
        else:
            tgt_elem = self.xml_seg.xpath('{}'.format('target'))[0]
            src_elem = self.xml_seg.xpath('{}'.format('source'))[0]
        self.xml_seg.remove(tgt_elem)
        self.xml_seg.insert(self.xml_seg.index(src_elem) + 1, target.xml_text)
        self.target = SegmentContent('target', self.xml_seg, self.wf_version)
        #print('Copy: ', self.target.all_text)
    
    def paste_target(self, target: SegmentContent):
        """
        Paste only the "text", keeping the original values if there was a target
        in the original file.
        
        Arguments:
            target (SegmentContent): The segment from where needs to be copied the target.
        """
        tgt_elem = None
        if self.wf_version == 'txlf':
            try:
                tgt_elem = self.xml_seg.xpath(
                    'Default:{}'.format('target'), namespaces=txlf_nsmap)[0]
            except IndexError:
                pass
            src_elem = self.xml_seg.xpath(
                    'Default:{}'.format('source'), namespaces=txlf_nsmap)[0]
        else:
            try:
                tgt_elem = self.xml_seg.xpath('{}'.format('target'))[0]
            except IndexError:
                pass
            src_elem = self.xml_seg.xpath('{}'.format('source'))[0]
        if tgt_elem is not None:
            for elem in tgt_elem.iterchildren():
                tgt_elem.remove(elem)
        else:
            if self.wf_version == 'txlf':
                tgt_elem = et.Element('{urn:oasis:names:tc:xliff:document:1.2}target', nsmap=txlf_nsmap)
            else:
                tgt_elem = et.Element('target')
            self.xml_seg.insert(self.xml_seg.index(src_elem) +1, tgt_elem)
        
        tgt_elem.text = target.xml_text.text
        if len(list(target.xml_text.iterchildren())) > 0:
            for elem in target.xml_text.iterchildren():
                tgt_elem.append(elem)

    @property
    def is_hidden(self):
        """
        Property of status of the segment. If it is in some way "hidden" or is not.
        It can have this flag enabled in the case of TXLF if the segment is locked or
        the flag translate is setted.
        """
        return self._hidden

    def hide(self, lock: bool=False) -> None:
        """
        Hide a segment. If is 'txml' it hides the segment adding a comment.
        Check what extension is to make one implentation or other.
        """
        if not self._hidden:
            self._hidden = True
            if self.wf_version == 'txml':
                self.__hide_txml()
            elif self.wf_version == 'txlf':
                if lock:
                    self.locked = True
                else:
                    self.hidden = True

    def unhide(self, lock: bool=False):
        """
        Unhide a segment
        """
        if self.wf_version == 'txml':
            self.__unhide_txml()
        else:
            if lock:
                self.locked = False
            else:
                self.hidden = False
        self._hidden = False

    def __unhide_txml(self):
        """
        Method to unhide the TXML segment.
        This method uncomments the TXML segment.
        """
        
        if self.xml_comment is not None:
            new_elem = et.XML(self.xml_comment.text)
            idx = self.xml_comment.getparent().index(self.xml_comment)
            com_parent = self.xml_comment.getparent()
            elements = [element for element in com_parent.iterchildren()]
            elements[idx].getparent().remove(elements[idx])
            self.paragraph.xml_par.insert(idx, new_elem)
            self.xml_seg = self.paragraph.xml_par[idx]
            self.xml_comment = None
            self._hidden = False

    def __hide_txml(self) -> None:
        """
        Hide the segment of the TXML by commenting it
        """
        xml_parent = self.xml_seg.getparent()
        try:
            xml_parent.replace(self.xml_seg, et.Comment(et.tostring(self.xml_seg)))
        except ValueError:
            print(self.id)
    
    def pseudotranslate(self):
        """
        Pseudotranslate the Segment copying the source to the target and replacing the vowels with random accented words.
        """
        replacements = {
            'a': ['á', 'à', 'ä'], 
            'e': ['é', 'è', 'ê'], 
            'i': ['í', 'ï', 'î'], 
            'o': ['ó', 'ò', 'ö'], 
            'u': ['ú', 'û', 'ü'], 
            'A': ['Á', 'À', 'Ä'], 
            'E': ['É', 'È', 'Ë'], 
            'I': ['Í', 'Î', 'Î'], 
            'O': ['Ó', 'Ò', 'Ö'], 
            'U': ['Ú', 'Û', 'Ü']
            }
        srccopy = deepcopy(self.source)
        self.target.paste_segment_content(srccopy)
        #print(self.target.text)
        for repl in replacements:
            if self.target.xml.text:
                self.target.xml.text = self.target.xml.text.replace(
                                repl, random.choice(replacements.get(repl)) * random.choice(range(1, 3)))
            for elem in self.target.xml:
                if not 'bpt' in elem.tag and 'x' not in elem.tag and not'ept' in elem.tag:
                    if elem.text:
                        elem.text = elem.text.replace(repl, random.choice(
                                replacements.get(repl))*random.choice(range(1, 3)))
                if elem.tail:
                    elem.tail = elem.tail.replace(repl, random.choice(
                            replacements.get(repl))*random.choice(range(1, 3)))
    
    def pseudotranslate2(self):
        """
        Pseudotranslate reversing the strings
        Returns:
            None
        """
        if self.target.xml.text:
            self.target.xml.text = self.target.xml.text[::-1]
        for elem in self.target.xml:
            if not 'bpt' in elem.tag and 'x' not in elem.tag and not 'ept' in elem.tag:
                if elem.text:
                    elem.text = elem.txt[::-1]
            if elem.tail:
                elem.tail = elem.tail[::-1]
    
    async def async_pseudotranslate(self):
        """
        Pseudotranslate the Segment copying the source to the target and replacing the vowels with random accented words.
        """
        replacements = {
            'a': ['á', 'à', 'ä'], 
            'e': ['é', 'è', 'ê'], 
            'i': ['í', 'ï', 'î'], 
            'o': ['ó', 'ò', 'ö'], 
            'u': ['ú', 'û', 'ü'], 
            'A': ['Á', 'À', 'Ä'], 
            'E': ['É', 'È', 'Ë'], 
            'I': ['Í', 'Î', 'Î'], 
            'O': ['Ó', 'Ò', 'Ö'], 
            'U': ['Ú', 'Û', 'Ü']
            }
        srccopy = deepcopy(self.source)
        self.target.paste_segment_content(srccopy)
        #print(self.target.text)
        for repl in replacements:
            if self.target.xml.text:
                self.target.xml.text = self.target.xml.text.replace(
                                repl, random.choice(replacements.get(repl)) * random.choice(range(1, 3)))
            for elem in self.target.xml:
                if not 'bpt' in elem.tag and 'x' not in elem.tag and  not'ept' in elem.tag:
                    if elem.text:
                        elem.text = elem.text.replace(repl, random.choice(
                                replacements.get(repl))*random.choice(range(1, 3)))
                if elem.tail:
                    elem.tail = elem.tail.replace(repl, random.choice(
                            replacements.get(repl))*random.choice(range(1, 3)))
    def swap(self):
        """
        Swap the contents between source and target. The contents of target go to source
        and the contents of source go to target.
        """
        srccopy = deepcopy(self.source)
        tgtcopy = deepcopy(self.target)
        self.source.paste_segment_content(tgtcopy)
        self.target.paste_segment_content(srccopy)
    
    def jsonify(self):
        """
        Serialize the segment to JSON object
        """
        return {
            'id': self.id, 
            'locked': self.locked, 
            'wf_version': self.wf_version, 
            'xml': et.tostring(self.xml, encoding=str),
            'maxlen': self.maxlen,
            'seg_number': self.seg_number,
            'source': self.source.jsonify(),
            'target': self.target.jsonify() if not isinstance(self.target._xml, str) else ''
            }
