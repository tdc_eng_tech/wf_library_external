"""
Segments class containing Segment
"""
__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


import warnings

from .segment import Segment
from typing import Any
import re


class Segments:
    """
    Container class of Segment
    """
    def __init__(self, parent: Any=None, iterable: Any=None):
        """
        Initialization of the segments class
        """
        self.__segments = []
        self.__indexes = dict()
        self._index = 0
        self._last_item = len(self.__segments) - 1
        self._parent = parent
        if iterable:
            self.__initialize_segments(iterable)
    
    def __initialize_segments(self, iterable: Any):
        for item in iterable:
            if isinstance(item, Segment):
                self.add(item)
    
    def __getitem__(self,  key: Any):
        """
        """
        if isinstance(key, int):
            if key in self.__indexes:
                return self.__segments[key]
            else:
                raise IndexError(f'The index {key} is out of range')
        elif isinstance(key, slice):
            return self.__segments[key]
        else:
            for k, value in self.__indexes.items():
                if isinstance(key, str):
                    if key == value.id:
                        return self.__segments[k]
                elif value == key:
                    return self.__segments[k]
            raise KeyError(f'The segment file with key <{key}> does not exist.')
    
    def __iter__(self):
        return iter(self.__segments)
    
    def __next__(self):
        if self._index < len(self.__segments):
            self._index += 1
            return self.__segments[self._index]
        else:
            raise StopIteration('Exhausted the number of WF files in this collection. '
                                        'It was the last.')
    
    def __len__(self):
        return len(self.__segments)
    
    def __repr__(self):
        return f'<Segments {self.__segments}>'
    
    def __str__(self):
        return f'<Segments {[segment.id for segment in self.__segments]}>'
    
    def __bool__(self):
        if len(self.__segments) > 0:
            return True
        else:
            return False
    
    def __contains__(self, segment: Any):
        return self.has(segment)
    
    @property
    def trash_bin(self):
        return self.__trash_bin
    
    def add(self, segment: Segment):
        if isinstance(segment, Segment):
            idx = len(self.__segments)
            segment.segment_group = self
            self.__segments.append(segment)
            self.__indexes[idx] = segment
        else:
            raise TypeError(f'It must be an Segment object. It was {type(segment)}')
    
    def append(self, segment: Segment):
        """
        """
        warnings.warn('Deprecated method. Use add instead')
        self.add(segment)
    
    def items(self):
        """
        Return a tuple of the id of the segment ad the segment object.
        """
        for segment in self.__segments:
            yield segment.id, segment
    
    def index(self, segment_object: Segment):
        """
        Return the position of the segment in the Segments class.
        
        Arguments:
            segment_object (Segment): Segment to find where is located.
        """
        return self.__segments.index(segment_object)
    
    def delete(self, segment_object: Segment):
        """
        Remove a segment_object from the list and add it to the trash bin
        Arguments:
        segment_object: Object file or object_files to be deleted
        """
        if isinstance(segment_object, Segment):
            if self.__has_internal(str(segment_object.id)):
                self.__delete_internal(segment_object)
    
    def recover(self, segment_object: Segment):
        """
        Restore a removed Segment file from the trash bin
        """
        if isinstance(segment_object, Segment):
            if segment_object in self.trash_bin:
                self.__segments.append(segment_object)
                self.trash_bin.remove(segment_object)
        else:
            raise TypeError(f'TypeError: It has to be <Segment> and the type was {type(segment_object)}')
    
    def __delete_internal(self, segment_object):
        deleted = self.__segments[self.__segments.index(segment_object)]
        if deleted:
            self.__trash_bin.append(segment_object)
            #del self.__segments[segment_object]
            self.__segments.remove(segment_object)
    
    def __has_internal(self, segment_object: Any) -> Any:
        """
        Private method to check if a WF object is already in the list.
        """
        if isinstance(segment_object, Segment):
            return segment_object in self.__segments
        elif isinstance(segment_object, str):
            for id, segment in self.items():
                if id.lower == segment_object.id:
                    return True
            return False
        else:
            raise TypeError('Not recognized type to check.')
    
    def has(self, segment_object: Any):
        """
        Check if the segment_object already exist in SegmentGroup
        Arguments:
        segment_object: Object Segment to check if it is contained in the group or not
        """
        res_value = False
        if isinstance(segment_object, Segment):
            if self.__has_internal(str(segment_object.id)):
                res_value = True
        else:
            res_value = self.__has_internal(segment_object)
        return res_value
    
    def get(self, id: str, default=None) -> Segment:
        """
        Get a segment with a specific id. Emulates the get of the dictionaries get.
        """
        if isinstance(id, str):
            check_pat = re.compile('(?P<page>[0-9]+?)-(?P<par>[0-9]+?)-(?P<seg>[0-9]+)')
            res = check_pat.search(id)
            if res:
                if int(res.group('page')) > 0 and int(res.group('par')) > 0 and int(res.group('seg')) > 0:
                    return self[id]
                else:
                    return default
            else:
                return default
