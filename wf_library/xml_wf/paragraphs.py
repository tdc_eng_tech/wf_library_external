"""
Paragraphs class containing paragraph
"""
__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


import warnings

from .paragraph import Paragraph
from typing import Any
import re


class Paragraphs:
    """
    Container class of Paragraph
    """
    def __init__(self, parent: Any=None, iterable: Any=None):
        """
        Initialization of the paragraphs class
        """
        self.__paragraphs = []
        self.__indexes = dict()
        self._index = 0
        self._last_item = len(self.__paragraphs) - 1
        self._parent = parent
        if iterable:
            self.__initialize_paragraphs(iterable)
    
    def __initialize_paragraphs(self, iterable: Any):
        for item in iterable:
            if isinstance(item, Paragraph):
                self.add(item)
    
    def __getitem__(self,  key: Any):
        """
        """
        if isinstance(key, int):
            if key in self.__indexes:
                return self.__paragraphs[key]
            else:
                raise IndexError(f'The index {key} is out of range')
        elif isinstance(key, slice):
            return self.__paragraphs[key]
        else:
            for k, value in self.__indexes.items():
                if isinstance(key, str):
                    if key == value.id:
                        return self.__paragraphs[k]
                elif value == key:
                    return self.__paragraphs[k]
            raise KeyError(f'The paragraph file with key <{key}> does not exist.')
    
    def __iter__(self):
        return iter(self.__paragraphs)
    
    def __next__(self):
        if self._index < len(self.__paragraphs):
            self._index += 1
            return self.__paragraphs[self._index]
        else:
            raise StopIteration('Exhausted the number of WF files in this collection. '
                                        'It was the last.')
    
    def __len__(self):
        return len(self.__paragraphs)
    
    def __repr__(self):
        return f'<Paragraphs {self.__paragraphs}>'
    
    def __str__(self):
        return f'<Paragraphs {[paragraph.id for paragraph in self.__paragraphs]}>'
    
    def __bool__(self):
        if len(self.__paragraphs) > 0:
            return True
        else:
            return False
    
    def __contains__(self, paragraph: Any):
        return self.has(paragraph)
    
    @property
    def parent(self):
        """
        Container of the class
        """
        return self._parent
    
    @property
    def trash_bin(self):
        return self.__trash_bin
    
    def add(self, paragraph: Paragraph):
        if isinstance(paragraph, Paragraph):
            idx = len(self.__paragraphs)
            paragraph._parent = self
            self.__paragraphs.append(paragraph)
            self.__indexes[idx] = paragraph
        else:
            raise TypeError(f'It must be an Paragraph object. It was {type(paragraph)}')
    
    def append(self, paragraph: Paragraph):
        """
        """
        warnings.warn('Deprecated method. Use add instead')
        self.add(paragraph)
    
    def items(self):
        """
        """
        for paragraph in self.__paragraphs:
            yield paragraph.id, paragraph
    
    def index(self, paragraph_object: Paragraph):
        """
        """
        return self.__paragraphs.index(paragraph_object)
    
    def delete(self, paragraph_object: Paragraph):
        """
        Remove a paragraph_object from the list and add it to the trash bin
        Arguments:
        paragraph_objects: Object file or object_files to be deleted
        """
        if isinstance(paragraph_object, Paragraph):
            if self.__has_internal(str(paragraph_object.id)):
                self.__delete_internal(paragraph_object)
    
    def recover(self, paragraph_object: Paragraph):
        """
        Restore a removed Paragraph file from the trash bin
        """
        if isinstance(paragraph_object, Paragraph):
            if paragraph_object in self.trash_bin:
                self.__paragraphs.append(paragraph_object)
                self.trash_bin.remove(paragraph_object)
        else:
            raise TypeError(f'TypeError: It has to be <Paragraph> and the type was {type(paragraph_object)}')
    
    def __delete_internal(self, paragraph_object):
        deleted = self.__paragraphs[self.__paragraphs.index(paragraph_object)]
        if deleted:
            self.__trash_bin.append(paragraph_object)
            #del self.__paragraphs[paragraph_object]
            self.__paragraphs.remove(paragraph_object)
    
    def __has_internal(self, paragraph_object: Any) -> Any:
        """
        Private method to check if a WF object is already in the list.
        """
        if isinstance(paragraph_object, Paragraph):
            return paragraph_object in self.__paragraphs
        elif isinstance(paragraph_object, str):
            for id, paragraph in self.items():
                if id.lower == paragraph_object.id:
                    return True
            return False
        else:
            raise TypeError('Not recognized type to check.')
    
    def has(self, paragraph_object: Any):
        """
        Check if the paragraph_object already exist in ParagraphGroup
        Arguments:
        paragraph_object: Object Paragraph to check if it is contained in the group or not
        """
        res_value = False
        if isinstance(paragraph_object, Paragraph):
            if self.__has_internal(str(paragraph_object.id)):
                res_value = True
        else:
            res_value = self.__has_internal(paragraph_object)
        return res_value
    
    def get(self, id: str, default=None) -> Paragraph:
        """
        Get a paragraph with a specific id. Emulates the get of the dictionaries get.
        """
        if isinstance(id, str):
            check_pat = re.compile('paragraph-(?P<page>[0-9]+?)-(?P<par>[0-9]+)')
            res = check_pat.search(id)
            if res:
                if int(res.group('page')) > 0 and int(res.group('par')) > 0:
                    return self[id]
                else:
                    return default
            else:
                return default
