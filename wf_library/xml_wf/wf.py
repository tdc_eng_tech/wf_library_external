__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from pathlib import Path
from lxml import etree as et
import warnings


from .const import txlf_nsmap
from .paragraph import Paragraph
from .paragraphs import Paragraphs
from .segment import Segment
from .segments import Segments


class Wf:
    """
    It allows to process a WordFast file.
    
    Example:
        wf = Wf('file')
    
    Attributes:
        wf_file (str): Path string to the WordFast file to be processed.
        wf_file_path (Path): Path objecto the WordFast file path.
        xml_tree (xml_tree): XML tree of the WF file.
        xml_root (Element): Root element of the XML tree.
        paragraphs (list): List that contains the Paragraph objects.
        wf_group (WfGroup): References to the WfGroup class that contains the Wf file.
    
    Properties:
        source_lang (str): Source language of the file. 
        target_lang (str): Target language of the file. It can be empty.
        total_segment_count (int): Read-only property that return the total of segments.
    """
    def __init__(self, wf_file):
        """
        Initialize the class.
        """
        if Path(wf_file).suffix == '.txml' or Path(wf_file).suffix == '.txlf':
            self.wf_file = str(wf_file)
        else:
            raise ValueError('Wrong format file: It is not a WF valid file.')
        if Path(wf_file).is_file():
            xml_parser = et.XMLParser(encoding='utf-8')
            try:
                self.xml_tree = et.parse(self.wf_file, xml_parser)
            except et.XMLSyntaxError:
                xml_parserhf = et.XMLParser(encoding='utf-8', huge_tree=True)
                try:
                    self.xml_tree = et.parse(self.wf_file, xml_parserhf)
                except et.XMLSyntaxError:
                    self.xml_tree = et.parse(self.wf_file)
            self.xml_root = self.xml_tree.getroot()
            if self.wf_file_path.suffix == '.txml':
                self.wf_version = 'txml'
            elif self.wf_file_path.suffix == '.txlf':
                self.wf_version = 'txlf'
        else:
            raise FileNotFoundError(f'The file {wf_file} is not found. Please check the path.')
        #self._paragraphs = []
        self._paragraphs = Paragraphs(self)
        self.__target_language = ''
        self.__source_language = ''
        self.__original_file = ''
        self.__append_paragraphs()
        self._wf_group = None
        self.__total_segment_count = None
        self.__skeleton = None
    
    def __repr__(self):
        return f'<Wf {self.filename}>'
    
    def __enter__(self):
        """
        """
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        """
        self.save()
    
    @property
    def wf_file_path(self):
        """
        File Path of the XML file of the WordFast File (Wf file).
        """
        return Path(self.wf_file)
    
    @property
    def source_file(self):
        """
        """
        return str(Path(self.wf_file_path.parent) / self.wf_file_path.stem)
    
    @property
    def filename(self):
        """
        Name of the Wf file
        """
        return Path(self.wf_file).name
    
    @property
    def xml(self):
        """
        Access to the xml tree of the file
        """
        return self.xml_tree
    
    @xml.setter
    def xml(self, value: et.ElementTree):
        if isinstance(value, et.ElementTree):
            self.xml_tree = value
    
    def is_equal(self, wf):
        """
        Check if the structure of the file is the same or is the same file.
        
        Arguments:
            wf (Wf): Wf object to be compared with.
        """
        same = False
        if len(self.segments) == len(wf.segments):
            for seg1, seg2 in zip(self.segments, wf.segments):
                if seg1.attributes == seg2.attributes and \
                   seg1.source.text == seg2.source.text and \
                   seg1.target.text == seg2.target.text:
                       same = True
                else:
                    return False
        else:
            return False
        return same
    
    def append_group(self, wf_group):
        """
        Add the WordFast object to a WordFast group object. Making persistent the
        object.
        
        Arguments:
        wf_group (WfGroup): Group object where the Wf are added/stored.
        """
        warnings.warn('Deprecated, use property instead')
        self._wf_group = wf_group
    
    @property
    def wf_group(self):
        return self._wf_group
    
    @wf_group.setter
    def wf_group(self, gr):
        self._wf_group = gr
    
    def __append_paragraphs(self):
        """
        Private method to appends the paragraphs to the Wf file.
        """
        if self.wf_version == 'txlf':
            xml_paragraphs = self.xml_tree.xpath("//Default:group[@restype='x-paragraph']",namespaces=txlf_nsmap)
        else:
            xml_paragraphs = self.xml_tree.xpath('translatable')
        self._paragraphs = Paragraphs(self, (Paragraph(xml_paragraph,
                                             self.wf_version,
                                             parent=self.paragraphs,
                                             wf=self)
                                                for idx, xml_paragraph in enumerate(xml_paragraphs)))
        count = 1
        for paragraph in self._paragraphs:  
            for segment in paragraph.segments:
                segment.seg_number = count
                count +=1
    
    def __get_attribute(self, element,  attribute, nsmap = None):
        """
        Gets the sepecific attribute
        Arguments:
            element (Element): Element that needs to be processed
            attribute (str): Attribute string name to be processed
        """
        
        if nsmap:
            elements = self.xml_tree.xpath(element, namespaces = nsmap)
            
        else:
            elements = self.xml_tree.xpath(element)
        for element in elements:
            try:
               return element.attrib[attribute]
            except KeyError:
                return -1

    @property
    def skeleton(self):
        """
        Get the skeleton of the original file. Mainly for xml files and other
        text files.
        It is read from the XML file and added to the object.
        """
        if self.__skeleton:
            return self.__skeleton
        else:
            if self.wf_version == 'txlf':
                try:
                    skeleton = self.xml_tree.xpath('//Default:internal-file',
                                     namespaces=txlf_nsmap)[0].text
                except AttributeError:
                    pass
                except IndexError:
                    pass
            else:
                try:
                    skeleton = self.xml_tree.xpath('//skeleton')[0].text
                except AttributeError:
                    pass
                except IndexError:
                    pass
            self.__skeleton = skeleton
            return self.__skeleton
    
    @skeleton.setter
    def skeleton(self, value: str):
        assert isinstance(value, str)
        if value and isinstance(value, str):
            self.__skeleton = value
            if self.wf_version == 'txlf':
                try:
                    skeleton = self.xml_tree.xpath('//Default:internal-file', 
                                                namespaces=txlf_nsmap)[0]
                except IndexError:
                    self.__skeleton = None
            else:
                try:
                    skeleton = self.xml_tree.xpath('//skeleton')[0]
                except IndexError:
                    self.__skeleton = None
            if skeleton is not None:
                skeleton.text = value
        else:
            raise TypeError('The value is not a string. Please check the type.')
    
    @property
    def target_lang(self):
        """
        Reads the target locale from the TXML file.
        
        Return:
            target_lang (str): Return the target language.
        """
        if self.__target_language:
            return self.__target_language
        else:
            if self.wf_version == 'txlf':
                target_language = self.__get_attribute('//Default:file', 
                    'target-language',
                    nsmap=txlf_nsmap
                    )
            else:
                target_language = self.__get_attribute('//txml', 'targetlocale')
            if target_language != -1:
                self.__target_language = target_language
            else:
                return ''
            return self.__target_language

    @target_lang.setter
    def target_lang(self, lang):
        if '_' in lang:
            lang.replace('_', '-')
        if self.wf_version == 'txlf':
            txlf_element = self.xml_tree.xpath(
                    '//Default:file',
                    namespaces=txlf_nsmap)[0]
            try:
                txlf_element.attrib[
                'target-language'] = lang      
            except:
                return -1
        else:
            txml_elements = self.xml_tree.xpath('//txml')
            try:
                #Loads the element txml that is where are the languages defined
                for element in txml_elements:
                    element.attrib['targetlocale'] = lang
            except TypeError:
                return -1
        #tree = et.ElementTree(self.xml_tree)
        #tree.write(self.wf_file, encoding='utf-8', pretty_print = True)
        self.__target_language = lang
    
    @property
    def source_lang(self):
        """
        Get the source language from the object if it exists or from the associated
        file, if the language was not already set.
        
        Return:
            source_lang (str): Return the soruce language
        """
        if self.__source_language:
            return self.__source_language
        else:
            if self.wf_version == 'txlf':
                source_language = self.__get_attribute(
                    '//Default:file',
                    'source-language',
                    nsmap=txlf_nsmap
                    )
            else:
                source_language = self.__get_attribute('//txml', 'locale')
            if source_language != -1:
                self.__source_language = source_language
            else:
                return -1
            return self.__source_language
    
    @source_lang.setter
    def source_lang(self, lang):
        if '_' in lang:
            lang.replace('_', '-')
        if self.wf_version == 'txlf':
            txlf_element = self.xml_tree.xpath(
                    '//Default:file',
                    namespaces=txlf_nsmap)[0]
            try:
                txlf_element.attrib[
                'source-language'] = lang   
            except:
                return -1
        else:
            txml_elements = self.xml_tree.xpath('//txml')
            try:
                #Loads the element txml that is where are the languages defined
                for element in txml_elements:
                    
                    element.attrib['sourcelocale'] = lang
            except TypeError:
                return -1
#        tree = et.ElementTree(self.xml_tree)
#        tree.write(self.wf_file, encoding='utf-8', pretty_print = True)
        self.__source_language = lang
    
    @property
    def original_file(self):
        """
        Get the file from what was generated the WF file.
        
        return:
            original_file: Return the path to the source file from which was generated the Wf file.
        """
        if not self.__original_file:
            if self.wf_version == 'txlf':
               
                self.__original_file = self.__get_attribute('//Default:file', 
                                'original', nsmap=txlf_nsmap)

            else:
                self.__original_file = self.__get_attribute('txml', 'file_name')
        return self.__original_file
    
    @property
    def paragraphs(self):
        """
        Read-only property to access to the paragraphs of the Wf file.
        """
        return self._paragraphs
    
    @property
    def total_segment_count(self):
        """
        Get the number total of segments that exist in the TXLF file
        
        return:
            original_file: Return the path to the source file from which was generated the Wf file.
        """
        if not self.__total_segment_count:
            if self.wf_version == 'txlf':
               
                self.__total_segment_count = self.__get_attribute('//Default:file', 
                                '{http://www.gs4tr.org/schema/xliff-ext}total-segment-count', nsmap=txlf_nsmap)

            else:
                self.__total_segment_count = self.__get_attribute('txml', 'file_name')
        return self.__total_segment_count

    @property
    def segments(self) -> list:
        """
        Return all the segments of the file.
        """
        return Segments(self, (seg for par in self.paragraphs for seg in par.segments))
#        return [seg for par in self._paragraphs
#                  for seg in par.segments]

    def update_root(self, xml_root):
        """
        Allows to update the root for writing reasons.
        """
        self.xml_root = xml_root
    
    def save(self, file: str='') -> None:
        """
        Save the xml structure of the file. If it is a TXLF is saved as the wf_type.
        This method automatically adds the corresponding extension based on the wf_version.
        Also, allows to save_as if a name is given, otherwise is overwritten.
        It makes deprecated save_xml and save_as methods.
        Arguments:
            file (str): The file name with the file has to be saved. It should go without extension.
        """
        tree = et.ElementTree(self.xml_root)
        if not file:
            filepath = self.wf_file
        else:
            filepath = Path(f'{file}')
        tree.write(str(filepath), encoding='utf-8', pretty_print=True, 
                      xml_declaration=True)
    
    def save_xml(self):
        """
        Writes the new XML into the file. Save all the changes done in the XML structure.
        Deprecated
        """
        warnings.warn('This method is deprecated, use save instead', DeprecationWarning)
        tree = et.ElementTree(self.xml_root)
        tree.write(str(self.wf_file), encoding='utf-8', pretty_print=True, 
                 xml_declaration=True)

    def save_as(self, filename: str):
        """
        Write the new XML into a new file. Save as new name
        Deprecated
        
        Arguments:
            filename (str): New filename
        """
        warnings.warn('This method is deprecated, use save instead', DeprecationWarning)
        tree = et.ElementTree(self.xml_root)
        tree.write(str(Path(filename)), encoding='utf-8', pretty_print=True, 
                     xml_declaration=True)

    def jsonify(self):
        """
        Serialize Wf object to a JSON object.
        """
        return {
            'source_lang': self.source_lang, 
            'target_lang': self.target_lang, 
            'original_file': str(self.original_file),
            'wf_file': str(self.wf_file_path), 
            'wf_version': self.wf_version,
            'paragraphs': [par.jsonify() for par in self._paragraphs], 
            'segments': [seg.jsonify() for seg in self.segments]
            }

    def find(self, text: str, pos: str='source', case_sensitive: bool=True) -> Segment:
        """
        Find a text in the source, target or both. Return the segment containing this.
        
        Arguments:
            text (str): Text to be searched
            pos (str): Look for in source, target or both. Keywords: source, target, any
            case_sensitive(bool): Flag if the search should be case sensitive or not.
        """
        if not case_sensitive:
            text = text.upper()
        for segment in self.segments:
            if pos == 'source':
                check_text = segment.source.text
            if pos == 'target':
                check_text = segment.target.text
            if pos == 'any':
                check_text = f'{segment.source.text} {segment.target.text}'
            if not case_sensitive:
                check_text = check_text.upper()
            if text in check_text:
                return segment
    
    def findall(self, text: str, pos: str='source', case_sensitive: bool=True) -> list:
        """
        Find a text in the source, target or any and return a list of all segments containing that text.
        
        Arguments:
            text (str): Text to be searched
            pos (str): Look for in source, target or any. Keywords: source, target, any
            case_sensitive (bool): Flag to check if search is sensitive to case or not.
        """
        segments = []
        if not case_sensitive:
            text = text.upper()
        for segment in self.segments:
            if pos == 'source':
                check_text = segment.source.text
            if pos == 'target':
                check_text = segment.target.text
            if pos == 'any':
                check_text = f'{segment.source.text} {segment.target.text}'
            if not case_sensitive:
                check_text = check_text.upper()
            if text in check_text:
                segments.append(segment)
        return segments
    
    def get_paragraph(self, id: str) -> Paragraph:
        """
        Get a paragraph with specific id
        
        Arguments:
            id (str): Id of the paragraph
        """
        return self.paragraphs.get(id)
    
    def get_segment(self, id: str) -> Segment:
        """
        Get a segment with specific id
        
        Arguments:
            id(str): Id of the segment
        """
        return self.segments.get(id)
    
    def pseudotranslate(self):
        """
        Pseudotranslate the file with replacements if is an occidental language.
        """
#        from concurrent.futures import ThreadPoolExecutor
#        import concurrent.futures
#        with ThreadPoolExecutor(max_workers=10) as executor:
#            future_to_pseudo = {executor.submit(segment.pseudotranslate): segment for segment in self.segments}
#            for future in concurrent.futures.as_completed(future_to_pseudo):
#                future.result()
        for segment in self.segments:
            segment.pseudotranslate()
    
    def pseudotranslate2(self):
        """
        Pseudotranslate the document reversing the target strings.
        Returns:
            None
        """
        for segment in self.segments:
            segment.pseudotranslate2()
    
    async def pseudotranslate_async(self):
        """
        Asynchronous pseudotranslate
        """
        import asyncio
        for segment in self.segments:
            task = asyncio.create_task(segment.async_pseudotranslate(), name=str(segment.id))
            print(task)
    
    def _pseudotranslate_partial(self, start: int, end: int):
        for segment in self.segments[start:end]:
            segment.pseudotranslate()
    
    def pseudotranslate_thread(self):
        from concurrent.futures import ThreadPoolExecutor
        #from concurrent import futures
        if len(self.segments) > 50:
            with ThreadPoolExecutor(max_workers=10) as executor:
                for idx in range(0, len(self.segments), 50):
                    executor.submit(self._pseudotranslate_partial(idx, idx+50))

    def swap(self):
        """
        Swap source and target
        """
        src_lang = self.source_lang
        if self.target_lang:
            self.source_lang = self.target_lang
        self.target_lang = src_lang
        for segment in self.segments:
            segment.swap()
