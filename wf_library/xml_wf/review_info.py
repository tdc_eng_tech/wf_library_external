__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from lxml import etree as et

from .const import txlf_nsmap
from .grade import Grade
from .note import Note


class ReviewInfo:
    """
    Class that represents the information of Review (element gs4tr:review).
    Only TXLF.
    """
    def __init__(self, xml_review: et.Element, wf_version: str, parent: object=None):
        """
        Initialize the object ReviewInfo
        """
        self._xml = xml_review
        self._parent = parent
        self._wf_version = wf_version
        self._user = None
        self._timestamp = None
        self._tool_name = None
        self._tool_version = None
        self._grades = self.__load_grades()
        self._notes = self.__load_notes()
    
    def __load_grades(self):
        grades = self._xml.xpath('gs4tr:grade',  namespaces=txlf_nsmap)
        return [Grade(grade, self._wf_version, self) for grade in grades]
    
    def __load_notes(self):
        return [Note(note, self.wf_version, note.text, self) for note in self._xml.xpath(
                    'gs4tr:note', namespaces=txlf_nsmap)]
    
    @property
    def xml(self):
        return self._xml
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def wf_version(self):
        return self._wf_version
    
    @property
    def user(self):
        if not self._user:
            self._user = self.xml.get('from')
        return self._user
    
    @user.setter
    def user(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('from', value)
     
    @property
    def timestamp(self):
        if not self._timestamp:
            self._timestamp = self.xml.get('timestamp')
        return self._timestamp
    
    @timestamp.setter
    def timestamp(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('from', value)
    
    @property
    def tool_name(self):
        if not self._tool_name:
            self._tool_name = self.xml.get('tool-name')
        return self._tool_name
    
    @tool_name.setter
    def tool_name(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('tool-name', value)
     
    @property
    def tool_version(self):
        if not self._tool_version:
            self._tool_version = self.xml.get('tool-version')
        return self._tool_version
    
    @tool_version.setter
    def tool_version(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('from', value)
     
    @property
    def grades(self):
        return self._grades
    
    @property
    def notes(self):
        return self._notes
    
    def delete_grades(self):
        """
        """
        self._grades = []
        for grade in self._xml.xpath('gs4tr:grade', namespaces=txlf_nsmap):
            grade.getparent().remove(grade)
    
    def delete_notes(self):
        """
        """
        self._notes = []
        for note in self._xml.xpath('gs4tr:note', namespaces=txlf_nsmap):
            note.getparent().remove(note)
    
    def delete_note(self, snid: int=0):
        """
        """
        for note in self._notes:
            if note.id == snid:
                note._xml.getparent().remove(note._xml)
                self._notes.remove(note)
    
    def add_grade(self, score: str, grade: str):
        """
        Add a grade to the review.
        
        Arguments:
            score (str): The severity of the error is inserted here.
            grade (str): What type of error was done.
        """
        grade_xml = et.SubElement(self._xml, '{http://www.gs4tr.org/schema/xliff-ext}grade')
        id = len(self.grades)
        grade_xml.attrib['gId'] = str(id)
        grade_xml.attrib['score'] = str(score)
        grade_xml.attrib['grade'] = grade
        self._grades.append(Grade(grade_xml, self.wf_version, self))

    def add_note(self, value: str, note_type: str='segment-note-type'):
        """
        Add a note to the review
        """
        note_xml = et.SubElement(self._xml, '{http://www.gs4tr.org/schema/xliff-ext}note')
        id = len(self.notes)
        note_xml.set('snid', str(id)) 
        note_xml.set('segment-note-type', note_type)
        note_xml.text = str(value)
        self._notes.append(Note(note_xml, self.wf_version, parent=self))
