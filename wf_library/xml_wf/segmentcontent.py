__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from lxml import etree as et
import re
import warnings
from copy import deepcopy

from .const import txlf_nsmap
from ..generic.gfunctions import validate_timestamp


class SegmentContent:
    """
    Composition class for the segment. It allows to create the source or/and
    the target elements in the XML structure of the WF files (TXML and TXLF).
    Here is where is stored the translatable and translated content.
    
    Arguments:
        value (str): It only accepts 2 values: source or target.
        xml_seg (et.Element): XML Element of the Segment. Part of ElementTree.
        wf_version (str): Version of WordFast. 'txml' or 'txlf'
    """
    def __init__(self, value, xml_seg, wf_version, parent: object=None):
        """
        Initialization of the file.
        Arguments:
            value(str): It only accepts 2 values: source or target
        """
        self._xml_seg = xml_seg
        self.value = value
        self._xml = ''
        self.wf_version = wf_version
        self._text = ''
        self._only_text = ''
        self._xml_string = ''
        self.__attributes = dict()
        self.__seg_info = dict()
        self._parent = parent
        self._tags = dict()
        try:
            if self.wf_version == 'txlf':
                    self._xml = self._xml_seg.xpath(
                            'Default:{}'.format(value), namespaces=txlf_nsmap)[0]
            else:
                self._xml = self._xml_seg.find('{}'.format(value))
                #self._xml = self._xml_seg.xpath('{}'.format(value))[0]
                #print(self._xml)
            self._seginfo = self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}seginfo')
        except:
            self._xml_string = ''
            self._xml = ''
            self._only_text = ''
            self._attributes = dict()
            self._text = ''
            self._tags = dict()
    
    def __repr__(self):
        return f'<SegmentContent {self.value}>'
    
    @property
    def parent(self):
        """
        """
        return self._parent
    
    @property
    def xml_string(self):
        """
        """
        if self._xml is not None:
            
            try:
                return et.tostring(self._xml)
            except TypeError:
                return self._xml
        else: 
            return ''

    @property
    def attributes(self):
        """
        List the attributes of the SegmentContent, target or source.
        """
        if not self.__attributes:
            if self._xml is not None:
                for attribute, value in self._xml.attrib.items():
                    self.__attributes[attribute] = value
        else:
            return self.__attributes
    
    def get_attribute(self, attribute):
        """
        Get the value of the indicated attribute.
        Arguments:
            attribute (str): Name of the attribute to retrieve.
        Return:
            value(str): The value of the indicated attribute.
            If it is empty or does not exist return None.
        """
        if not self.__attributes and self._xml is not None:
            for attr, value in self._xml.attrib.items():
                if attr == attribute:
                    return value
        elif self.__attributes:
            for attr,  value in self.__attributes.items():
                if attr == attribute:
                    return value
        elif not self.__attributes and self._xml is None:
            #print(self._xml, self._xml_seg.find('target'))
            return None
    
    def set_seginfo(self, username: str=None, timestamp: str=None,
                    tool_name: str= None, tool_version: str=None, set_revision: str=None) -> str:
        """
        Add seginfo to the file
        Args:
            username (str):
                Optional; DESCRIPTION Defaults to None.
            timestamp (str):
                Optional; DESCRIPTION Defaults to None.
            tool_name (str):
                Optional; DESCRIPTION Defaults to None.
            tool_version (str):
                Optional; DESCRIPTION Defaults to None.
            set (str):
                Optional; DESCRIPTION Defaults to None.
        
        Returns:
            None
        """
        if self.seginfo:
            root = et.fromstring(self.seginfo)
        else:
            root = et.Element('root')
        if username:
            root.set('username', username)
        if timestamp:
            if validate_timestamp(timestamp):
                root.set('timestamp', timestamp)
            else:
                raise ValueError(f'The timestamp applied is not valid: {timestamp}.'
                                 'It should follow this format: "%Y%m%dT%H%M%SZ".')
        if tool_name:
            root.set('tool-name', tool_name)
        if tool_version:
            root.set('tool-version', tool_version)
        if set_revision:
            root.set('set', set_revision)
        self._xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', et.tostring(root, 
                                                                                    encoding='unicode'))
        return et.tostring(root, encoding='unicode')
    
    def get_seg_info(self):
        """
        Get seg_info full string for TXLF files
        """
        if self.wf_version == 'txlf':
            return self.xml.get('{http://www.gs4tr.org/schema/xliff-ext}seginfo')
        else:
            raise NotImplementedError('This function is not supported for TXML files')
    
    @property
    def seginfo(self) -> str:
        """
        """
        if self._seginfo:
            return self._seginfo
    
    @property
    def seginfo_dict(self) -> dict:
        """
        Return the seginfo as a dictionary
        """
        xml_seginfo = et.fromstring(self._seginfo)
        return xml_seginfo.attrib
    
    @property
    def username(self):
        """
        Get the username that modified this target.
        """
        if self.wf_version == 'txlf':
            seg_info_xml = et.fromstring(self.get_seg_info())
            return seg_info_xml.get('username')
        else:
            return self.xml.get('creationid')
    
    @username.setter
    def username(self, value: str):
        if self.wf_version == 'txlf':
            seginfo = et.fromstring(self.xml.get('{http://www.gs4tr.org/schema/xliff-ext}seginfo'))
            seginfo.set('username', value)
            self.xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', et.tostring(seginfo))
            self._seginfo = seginfo
        else:
            self.xml.set('creationid', value)
    
    @property
    def timestamp(self) -> str:
        """
        Return the timestamp of the creation of the Revision that is extracted from seginfo.
        """
        if self.wf_version == 'txlf':
            if self.seginfo_dict:
                return self.seginfo_dict.get('timestamp')
    
    @timestamp.setter
    def timestamp(self, value: str):
        if self.wf_version == 'txlf':
            if self._seginfo:
                xml_seg = et.fromstring(self._seginfo)
                xml_seg.set('timestamp', value)
                seginfo = et.tostring(xml_seg)
                self.xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', seginfo)
                self._seginfo = seginfo
        else:
            raise NotImplementedError('Username is not supported in TXML files.')
    
    def set_attribute(self, attribute, value):
        """
        Add or modidfy an existing attribute.
        Warning: If an attribute needs to be added to a TXLF file,
        it is important to know if the attribute has namespaces, 
        for example gs4tr:score.
        The namespaces for gs4tr would be {http://www.gs4tr.org/schema/xliff-ext}.
        So to add the attribute score in a TXLF file would be done as 
        the following example:
        
        Example:
            set_attribute('{http://www.gs4tr.org/schema/xliff-ext}score', '100')
        
        Arguments:
            attribute (str): Attribute to modify or add.
        """
        self.__attributes[attribute] = value
        if self._xml is not None:
            self._xml.attrib[attribute] = value
    
    def delete_attribute(self, attribute):
        """
        Delete an attribute.
        Warning: If an attribute needs to be added to a TXLF file,
        it is important to know if the attribute has namespaces, 
        for example gs4tr:score.
        The namespaces for gs4tr would be {http://www.gs4tr.org/schema/xliff-ext}.
        So to add the attribute score in a TXLF file would be done as 
        the following example:
        
        Example:
            delete_attribute('{http://www.gs4tr.org/schema/xliff-ext}score')
        
        Arguments:
            attribute (str): Attribute to modify or add.
        """
        if self._xml is not None:
            #print(attribute, self._xml.attrib)
            if attribute in self._xml.attrib:
                del self._xml.attrib[attribute]
    
    def delete_all_attributes(self):
        """
        Delete all attributes of the SegmentContent. It is used to leave
        without any attribute the <target> element.
        """
        if self._xml is not None:
            for key in self._xml.attrib.keys():
                del self._xml.attrib[key]
    
    def convert_ws_to_string(self):
        """
        """
        if self.wf_version == 'txlf':
                ws_tag = r'{http://www.gs4tr.org/schema/xliff-ext}ws'
        else:
            ws_tag = 'ws'
        for elem in self._xml_seg:
            if elem.tag == ws_tag:
                if len(self.xml) > 0:
                    if self.xml[-1].tail:
                        self.xml[-1].tail += elem.text
                    else:
                        self.xml[-1].tail = elem.text
                else:
                    print('Element:', self.xml, elem.text,  elem)
                    self.xml.text = f'{self.xml.text}{elem.text}'
                    print('Convert ws to string', self.xml)
                elem.getparent().remove(elem)
    
    @property
    def __text(self):
        """
        Get the text including the tags
        """
        warnings.warn('Old text implementation. To be deleted in future iterations', DeprecationWarning)
        if self._xml is not None and self._xml != '':
            xml_temp = self.__nodes_browse(False)
            return xml_temp.text
        else:
            return ''

    @property
    def text(self) -> str:
        """
        Get the text without tags. Replacing them with its internal text if it is applicable.
        """
        return self.__text_visualize()
    
    @text.setter
    def text(self, value):
        if self._xml is not None and self._xml != '':
            for elem in self._xml.iterchildren():
                elem.getparent().remove(elem)
        else:
            self._xml = et.SubElement(self._xml_seg, self.value)
        self._xml.text = value
    
    @property
    def text_only(self) -> str:
        """
        """
        return self.__text_visualize(True, False)
    
    @property
    def text_tagged(self) -> str:
        """
        Get the text with the tags without replacing them with its internal text.
        """
        return self.__text_visualize(delete_text=True, replace_tag=True)
    
    @property
    def text_tagged_xml(self) -> str:
        """
        Get the text with its full xml contents mainly for UMTApi.
        
        Example of response:
        'Text with tags <ph xmlns="urn:oasis:names:tx:xliff:document:1.2 ctype="x-regex" id="1">&lt;regex captured="[things]"/&gt;</ph>'
        """
        xml_temp = deepcopy(self.xml)
        text = ''
        if len(xml_temp) > 0:
            text = xml_temp.text if xml_temp.text else ''
            for elem in xml_temp:
                text += et.tostring(elem).decode('utf-8')
        return text
    
    @property
    def __all_text(self):
        """
        It displays the text including the text between tags.
        This function may be useful to calculate the maxlen of
        a sentence or similar.
        """
        warnings.warn('It is deprecated and hidden', DeprecationWarning)
        if self._xml is not None:
            xml_temp = self.__nodes_browse_text()
            return xml_temp.text
        else:
            return ''

    @property
    def text_tagged_ws(self) -> str:
        """
        Return the text with trailing spaces and without replacing tags.
        """
        if self.text_tagged:
            if self.wf_version == 'txlf':
                ws_tag = r'{http://www.gs4tr.org/schema/xliff-ext}ws'
            else:
                ws_tag = 'ws'
            full_all_text = ''
            for elem in self._xml_seg.iterchildren():
                if elem.tag == ws_tag:
                    if elem.text:
                        #print(elem.text)
                        full_all_text += elem.text
                if elem.tag == self._xml.tag:
                    full_all_text += self.text_tagged
            return full_all_text
        else:
            return ''
    
    @property
    def text_ws(self):
        """
        Display all the text including leading and trailing spaces. 
        Those are the spaces included in the <ws> tags in TXLF or TXML files.
        """
        if self.text:
            if self.wf_version == 'txlf':
                ws_tag = r'{http://www.gs4tr.org/schema/xliff-ext}ws'
            else:
                ws_tag = 'ws'
            full_all_text = ''
            for elem in self._xml_seg.iterchildren():
                if elem.tag == ws_tag:
                    if elem.text:
                        #print(elem.text)
                        full_all_text += elem.text
                if elem.tag == self._xml.tag:
                    full_all_text += self.text
            return full_all_text
        else:
            return ''
    
    @property
    def target_score(self) -> int:
        """
        The target score that has the segment. It indicates its leverage.
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return int(self.get_attribute(
                                             '{http://www.gs4tr.org/schema/xliff-ext}score')) if self.get_attribute(
                                             '{http://www.gs4tr.org/schema/xliff-ext}score') else 0
                else:
                    return int(self.get_attribute('score')) if self.get_attribute('score') else 0
        except KeyError:
            return None
    
    @target_score.setter
    def target_score(self, value: int):
        if self.value == 'target' and isinstance(value, int):
            if self.wf_version == 'txlf':
                self.xml.set('{http://www.gs4tr.org/schema/xliff-ext}score', str(value))
            else:
                self.xml.set('score', str(value))
   
    @property
    def rep_score(self):
        """
        The score of internal fuzzies/repetitions.
        Only supported in TXLF files.
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return int(self.get_attribute(
                                              '{http://www.gs4tr.org/schema/xliff-ext}repscore'))
                else:
                    raise TypeError('This property is not supported in TXML files.')
        except KeyError:
            return None
    
    @property
    def score(self):
        """
        Alias of target_score
        """
        return self.target_score
    
    @score.setter
    def score(self, value: int):
        self.target_score = value
    
    @property
    def state_qualifier(self):
        """
        Returns the qualifier of the contents. It is normally with the values:
            [exact-match, x-context-match, fuzzy-match]
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return self.get_attribute('state-qualifier')
                else:
                    raise TypeError('Property not supported for TXML files')
        except KeyError:
            return None
    
    @state_qualifier.setter
    def state_qualifier(self, value: str):
        if self.value == 'target':
            if self.wf_version == 'txlf':
                self.xml.set('state-qualifier', value)
            else:
                raise TypeError('Property cannot be set in TXML files. Only TXLF is supported.')
        else:
            raise ValueError('Only settable in target SegmentContent subtype.')
    
    @property
    def state(self) -> str:
        """
        Get the state of a translation (target).
        Returns:
            str: State of the translation. ['needs-translation', 'reviewed',...]
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return self.get_attribute('state')
                else:
                    raise TypeError('Property not supported for TXML files')
        except KeyError:
            return None
    
    @state.setter
    def state(self, value: str):
        if self.value == 'target':
            if self.wf_version == 'txlf':
                self.xml.set('state', value)
            else:
                raise TypeError('Property cannot be set in TXML files. Only TXLF is supported.')
        else:
            raise ValueError('Only settable in target SegmentContent subtype.')
    
    @property
    def xml_text(self):
        """
        """
        warnings.warn('This property is deprecated use xml instead. Just an alias.', DeprecationWarning)
        return self.xml
    
    @xml_text.setter
    def xml_text(self, xml_text: et._Element):
        warnings.warn('This property is deprecated use xml instead. Just an alias.', DeprecationWarning)
        self.xml = xml_text
    
    @property
    def xml(self):
        """
        Shows and allows to update the xml of theSegmentContent.
        """
        if self._xml is not None:
            return self._xml
        else:
            return None
    
    @xml.setter
    def xml(self, xml_text: et._Element):
        if isinstance(xml_text, et._Element):
            self._xml = xml_text
        else:
            raise Exception.TypeError('The type is not valid. It should be an XML Element')
    
    @property
    def phase_name(self):
        """
        Information of the phase of current contents.
        For example, Translation-1, Revision-1,...
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return self.get_attribute('phase-name')
                else:
                    raise TypeError('Property not supported for TXML files')
        except KeyError:
            return None
    
    @phase_name.setter
    def phase_name(self, value: str):
        if isinstance(value, str):
            if self.value == 'target' and self.wf_version == 'txlf':
                self._xml.set('phase-name', value)
            else:
                raise ValueError('This property is not supported in this object.'
                                        ' Only target and TXLF files are supported.')
        else:
            raise TypeError(f'Wrong type. It should be <str> and it was {type(value)}')
    
    def replace_tag_keeping_tail(self, element):
        """
        Safely replace an element
        """
        self._preserve_tail_before_replacement(element)
        element.getparent().remove(element)
    
    def get_number_tags(self, text: str='SoftReturn'):
        """
        Find the number of tags that have the segment breaker content in the SegmentContent object.
        """
        counter = 0
        for tag in self._xml.getchildren():
            if text in tag.text:
                counter += 1
        return counter
    
    def _preserve_tail_before_replacement(self, node):
        if node.tail: # preserve the tail
            previous = node.getprevious()
            tail = r'{tag}' + str(node.tail)

            if previous is not None: # if there is a previous sibling it will get the tail
              
                if previous.tail is None:
                    
                    previous.tail =  tail
                else:
                    previous.tail = previous.tail + tail
            else: # The parent get the tail as text
                parent = node.getparent()
                if parent.text is None:
                    parent.text = tail
                else:
                    parent.text = parent.text + tail
    
    def remove_keeping_tail(self, element):
        """Safe the tail text and then delete the element"""
        # Solution from Stackoverflow:
        # https://stackoverflow.com/questions/42932828/how-delete-tag-from-node-in-lxml-without-tail
        self._preserve_tail_before_delete(element)
        element.getparent().remove(element)

    def _preserve_tail_before_delete(self, node):
        """
        
        """
        equiv_text = ''
        if node.tail: # preserve the tail
            previous = node.getprevious()
            if previous is not None: # if there is a previous sibling it will get the tail
                if previous.tail is None:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        equiv_text = ''
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        previous.tail = equiv_text + node.tail
                    else:
                        previous.tail = node.tail
                else:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        equiv_text = ''
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        previous.tail = previous.tail + equiv_text + node.tail
                    else:
                        previous.tail = previous.tail + node.tail
            else: # The parent get the tail as text
                parent = node.getparent()
                if parent.text is None:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        parent.text = equiv_text + node.tail
                    else:
                        parent.text = node.tail
                else:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        equiv_text = ''
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        parent.text = parent.text +equiv_text + node.tail
                    else:
                        parent.text = parent.text + node.tail
    
    def remove_keeping_text_tail(self, element):
        """Safe the tail text and then delete the element"""
        # Solution from Stackoverflow:
        # https://stackoverflow.com/questions/42932828/how-delete-tag-from-node-in-lxml-without-tail
        self._preserve_tail_text_before_delete(element)
        element.getparent().remove(element)
    
    def __text_visualize(self, delete_text: bool=False, replace_tag: bool=True):
        """
        Internal method to replace text
        """
        xml_temp = deepcopy(self.xml)
        tags = [node.tag for node in xml_temp]
        for node in xml_temp:
            if self.wf_version == 'txlf':
                if (node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'):
                    if node.attrib['ctype'] == 'x-tab':
                        node.text = '\t'
                    elif node.attrib['ctype'] == 'lb':
                        node.text = '\n'
                    elif ((node.attrib['ctype'].lower() == 'x-fontformat' 
                     or node.attrib['ctype'] == 'x-shape' or 'bookmark' in node.attrib.get('ctype'))
                     and node.text):
                        node.text = ''
                    elif node.attrib['ctype'].lower() == 'x-linefeed' and node.text:
                        node.text = '\n'
                    elif node.get('ctype').lower() == 'x-regex' and node.text:
                        node.text = node.text.replace('<regex captured="','').replace('"/>','')
                elif node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept':
                    node.text = ''
            else:
                if node.tag == 'ut':
                    if node.attrib['type'] == 'fontformat' or 'fontformat' in node.text:
                        node.text = ''
        if delete_text:
            if replace_tag:
                temp_text = ''
                if len(xml_temp) > 0:
                    temp_text = xml_temp.text if xml_temp.text else ''
                    for node in xml_temp:
                        temp_text += '{tag}'
                        temp_text += node.tail if node.tail else ''
                        xml_temp.remove(node)
                try:
                    xml_temp.text = temp_text
                except AttributeError:
                    pass
            else:
                try:
                    et.strip_elements(xml_temp, *tags, with_tail=False)
                except TypeError as e:
                    if 'str' in str(e):
                        pass
        else:
            try:
                et.strip_tags(xml_temp, *tags)
            except TypeError as e:
                if 'str' in str(e):
                    pass
        try:
            return xml_temp.text
        except AttributeError:
            return ''

    def _preserve_tail_text_before_delete(self, node):
        """
        Includes in the contents of the text the contents of its tags.
        """
        if node.tail: # preserve the tail
            previous = node.getprevious()
            node_text = ''
            if self.wf_version == 'txlf':
                if (node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'):
                    if node.attrib['ctype'] == 'x-tab':
                        node_text = '\t'
                    elif node.attrib['ctype'] == 'lb':
                        node_text = '\n'
                    elif ((node.attrib['ctype'].lower() == 'x-fontformat' 
                     or node.attrib['ctype'] == 'x-shape')
                     and node.text):
                        node_text = ''
                    elif  node.attrib['ctype'].lower() == 'x-linefeed' and node.text:
                        node_text = '\n'
                    elif node.get('ctype').lower() == 'x-unknown' and node.text:
                        node_text = node.text
                elif node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept':
                    node_text = ''
                else:
                    node_text = node.text
            else:
                if (node.tag == 'ut'):
                    if node.attrib['type'] == 'fontformat' or 'fontformat' in node.text:
                        node_text = ''
                    else:
                        node_text = node_text
            if previous is not None: # if there is a previous sibling it will get the tail
                if previous.text is None:
                    previous.text = node_text
                else:
                    previous.text = previous.text + node_text
                if previous.tail is None:
                    previous.tail = node.tail
                else:
                    previous.tail = previous.tail + node.tail
            else: # The parent get the tail as text
                parent = node.getparent()
                if parent.text is None:
                    if node_text:
                        parent.text = node_text + node.tail
                    else:
                        parent.text = node.tail
                else:
                    try:
                        parent.text = parent.text + node_text + node.tail
                    except TypeError:
                        if node_text and node.tail is None:
                            parent.text = parent.text + node_text
                        elif node_text is None and node.tail:
                            parent.text = parent.text + node.tail
    
    def get_tags(self):
        """
        Read the tags of the content.
        Store the tags in the attribute in the key-value tags dictionary.
        Store the id of the tag as the key and the text as the value.
        """
        xml_temp = et.fromstring(self.xml_string)
        self._tags = dict()
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                tag = dict()
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        tag_id = child.get('id')
                        tag.setdefault('id', tag_id)
                        tag.setdefault('text', child.text)
                        tag.setdefault('type', child.get('c-type'))
                        tag.setdefault('display', child.get('{http://www.gs4tr.org/schema/xliff-ext}display-text'))
                        self._tags[tag_id] = tag
            else:
                if child.tag == 'ut':
                    tag_id = child.get('x')
                    self._tags[tag_id] = {'id':  tag_id, 'text': child.text}
    
    @property
    def tags(self):
        """
        Return a dictionary of tags organized by id in the segment.
        """
        return self._tags
    
    @property
    def xml_tags(self):
        """
        Get the xml tags of the element.
        """
        tags = []
        for child in self.xml:
            tags.append(child)
        return tags
    
    def fix_subtags(self):
        """
        Fix <sub> tags adding the Processing Instruction before if it does not exist.
        This processing instruction assumes that is a link to the previous paragraph.
        """
        for tag in self.xml_tags:
            if len(tag) > 0:
                for subtag in tag:
                    if subtag.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub':
                        if subtag.getprevious() is None:
                            pi = et.ProcessingInstruction('sub', text=f'paragraphId="{self.parent.parent.previous.id}"')
                            subtag.addprevious(pi)
    
    def replace_tags(self, orig: str, repl: str, regex: bool=False):
        """
        Replace the text of a tag for another value.
        
        """
        for child in self._xml.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        if child.text == orig:
                            child.text = repl
            else:
                if child.tag == 'ut':
                    if child.text == orig:
                        child.text = repl
    
    def replace_text(self, text: str, repl: str):
        """
        Replace the text with a value. 
        """
        if self.xml_text.text:
            self.xml_text.text = str(self.xml_text.text).replace(text, repl)
        for elem in self.xml_text:
            if elem.tail:
                elem.tail = str(elem.tail).replace(text, repl)
    
    def insert_start_text(self, text: str):
        """
        Insert a text in the beginning of the text of the SegmentContent
        """
        if self.xml.text:
            self.xml.text = f'{text}{self.xml.text}'
        else:
            self.xml.text = text
    
    def __regex_replace_text_elem(self, tparent: et.Element, trepl: str, tsearch_pat: object, 
        attrib: dict={'id': '1', 'ctype': 'x-regex'}, tail: bool=False):
        """
        """
        if tail:
            text = tparent.tail
        else:
            text = tparent.text
        if text:
            match = tsearch_pat.search(text)
            if match:
                tag = et.SubElement(tparent, trepl, attrib=attrib)
                if tag.get('ctype') == 'x-regex':
                    text = match.group(0)
                    if tail:
                        tparent.tail = tsearch_pat.sub('', text)
                    else:
                        tparent.text = tsearch_pat.sub('', text)
                    tag.text = f'<regex captured="{text}" />'
    
    def replace_text_by_tag(self, tsearch: str,
                 tag: str='{urn:oasis:names:tc:xliff:document:1.2}ph',
                 ctype: str='x-regex', 
                 regex: bool=True):
        """
        Replace the text by a tag
        """
        if regex:
            tsearch_pat = re.compile(tsearch)
        if len(self._xml) == 0:
            if regex:
                self.__regex_replace_text_elem(self._xml, tag, tsearch_pat, {'id': '1', 'ctype': ctype})
        else:
            n = len(self._xml)
            for elem in self._xml.iter():
                if 'source' in elem.tag or 'target' in elem.tag:
                    if elem.tail:
                        if tsearch_pat.search(elem.tail):
                            n += 1
                            self.__regex_replace_text_elem(
                                                                        elem,tag,
                                                                        tsearch_pat,
                                                                        {
                                                                            'id': f'{n}',
                                                                            'ctype': ctype
                                                                        },
                                                                        True)
                    if elem.text:
                        if tsearch_pat.search(elem.tail):
                            n += 1
                            self.__regex_replace_text_elem(
                                                                        elem,tag,
                                                                        tsearch_pat,
                                                                        {
                                                                            'id': f'{n}',
                                                                            'ctype': ctype
                                                                        })
    
    def replace_tag(self, tag: et.Element, orig: str, repl: str) -> int:
        """
        Replace a tag
        """
        if tag.text == orig:
            tag.text = repl
            return 0
        else:
            return -1
    
    def yield_tags(self):
        """
        """
        for child in self._xml.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        yield child
            else:
                if child.tag == 'ut':
                    yield child
    
    def find_tail(self, char: str) -> dict:
        """
        Check if the tail is the specified character or string. It returns a dictionary with
        the id as key and the tail as value.
        
        Arguments: 
            char (str): Character or string to check.
        
        Return:
            tags (dict): Return a dictionary with the id as key and the tail text as value.
        """
        xml_temp = et.fromstring(self.xml_string)
        tags = dict()
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        tag_id = child.attrib['id']
                        if child.tail == char:
                            tags[tag_id] = char
            else:
                if child.tag == 'ut':
                    tag_id = child.attrib['x']
                    tags[tag_id] = char
        return tags
    
    def replace_tail(self, tags: dict) -> None:
        """
        Replace the specified tags with the indicated values in the dictionary tags.
        Return nothing.
        
        Arguments:
            tags (dict): Dictionary of the id of the tag and the tail value.
        
        """
        #xml_temp = et.fromstring(self.xml_string)
        for id, value in tags.items():
            for child in self._xml.iterchildren():
                if self.wf_version == 'txlf':
                    if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        if child.attrib['id'] == id:
                                child.tail = value
    
    def __nodes_browse(self, delete=True):
        """
        """
        xml_temp = et.fromstring(self.xml_string)
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        if delete:
                            self.remove_keeping_tail(child)
                        else:
                            self.replace_tag_keeping_tail(child)
            else:
                if child.tag == 'ut':
                    if delete:
                        self.remove_keeping_tail(child)
                    else:
                        self.replace_tag_keeping_tail(child)
        return xml_temp
    
    def __nodes_browse_text(self):
        """
        """
        
        xml_temp = et.fromstring(self.xml_string)
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        
                        self.remove_keeping_text_tail(child)
                        
            else:
                if child.tag == 'ut':
                    self.remove_keeping_text_tail(child)
        return xml_temp
    
    # Method upgraded to the new structure.
    def find_terms(self, pattern_text: str,  excluding_word: str=''):
        """
        Looks for a pattern and finds this text based on an attribute.
        Also it allows to exclude a word. Similar to the method of segment.
        But it only checks in the property only_text.
        Arguments:
            pattern_text (str): The pattern to be checked.
            excluding_word(str): Word that matches with the pattern. But
                        should not be included.
        """
        pattern = re.compile(pattern_text)
        items_found =  pattern.findall(self.only_text)
        if excluding_word:
            self.terms = [term for term in items_found if term != excluding_word]
        else:
            self.terms = items_found
        return self.terms

    def flash_find_terms(self, keywords, attribute):
        """
        Looks for the specified keywords. It is a faster alternative to find_terms
        when the whole word is needed to searched.
        Arguments:
            keywords: String or tuple or list of strings containing the keywords to search
            attribute: Where has to be looked for the keyword.
        """
        from flashtext.keyword import KeyWordProcessor
        keyword_processor = KeyWordProcessor()
        if isinstance(keywords, str):
            keyword_processor.add_keyword(keywords)
        if isinstance(keywords, (list, tuple)):
            for keyword in keywords:
                keyword_processor.add_keyword(keyword)
        if attribute:
            self.terms = keyword_processor.extract_keywords(attribute)
        return self.terms
    
    def check_pattern(self, pattern, mode):
        """
        Checks if the text follows the pattern. And return True or False.
        Arguments:
            pattern(str): Regex expression to be checked
            mode(int): The mode that has to be done the check. 
                            0: Only matches in the beginning of the pattern
                            1: Searchs the string pattern in the whole string
        """
        pat_obj = re.compile(pattern)
        if mode == 0:
            if pat_obj.match(self.only_text):
                return True
            else: 
                return False
        elif mode == 1:
            if pat_obj.search(self.only_text):
                return True
            else:
                return False

    def paste_segment_content(self, segcontent: object, overwrite: bool=True):
        """
        Paste a segment content into the actual segment content. It copies only its contents.
        It is useful when you need to swap contents for example.
        
        Arguments:
            segcontent (SegmentContent): The SegmentContent to be pasted (source or target)
            overwrite (bool): Overwrite the current contents or paste the new ones at the end, merge.
        """
        tgt_elem = self.xml
        if isinstance(tgt_elem, str):
            tgt_elem = et.SubElement(self._xml_seg, self.value)
            self.xml = tgt_elem
        
        if segcontent.wf_version == self.wf_version:
            if overwrite:
                if tgt_elem is not None:
                    for elem in tgt_elem.iterchildren():
                        tgt_elem.remove(elem)
                    tgt_elem.text = segcontent.xml.text
                    #print(tgt_elem.text)
            else:
                self.convert_ws_to_string()
                #print('First paste xml:', self.xml)
                if len(tgt_elem) > 0:
                    if tgt_elem[-1].tail:
                        tgt_elem[-1].tail += segcontent.xml.text
                    else:
                        tgt_elem[-1].tail = segcontent.xml.text
                else:
                    #print(tgt_elem, tgt_elem.text)
                    # Keep in vigilance this as possible source to errors.
                    try:
                        tgt_elem.text += segcontent.xml.text
                    except AttributeError:
                        return
            if len(segcontent.xml) > 0:
                segcontentxml = deepcopy(segcontent.xml)
                for elem in segcontentxml:
                    #print(f'Adding {elem}, {elem.tag}, {elem.text}')
                    tgt_elem.append(elem)
                    print(list(tgt_elem))
        else:
            raise TypeError('The type of the SegmentContent is not the same. '
                                 'It must be both TXLF or TXML.')
    
    def paste_xml_contents(self, cont: str):
        """
        Overwrite the contents with the xml in string format. It is the XML string of the source or target.
        It assumes that there is no <target> or <source> element. But it should work if they are already there.
        
        Arguments:
            cont (str): XML string containing the tags and the text.
        """
        tgt_elem = self.xml
        if len(tgt_elem) == 0:
            tgt = et.SubElement(self._xml_seg, 'target')
            self.xml = tgt
            tgt_elem = self.xml
        if '<target' not in cont and '<source' not in cont:
            cont = f'<target>{cont}</target>'
        xml_cont = et.fromstring(cont)
        text = xml_cont.text
        if tgt_elem is not None:
            for elem in tgt_elem.iterchildren():
                tgt_elem.remove(elem)
            tgt_elem.text = text
        for elem in xml_cont:
            tgt_elem.append(elem)

    def jsonify(self):
        """
        Convert the object into a JSON object
        """
        if self.xml_text is not None and not isinstance(self.xml_text, str):
            xml_text = et.tostring(self._xml,encoding=str)
        else:
            xml_text = ''
        return {
            'attributes' : self.attributes, 
            'wf_version': self.wf_version, 
            'text_tagged': self.text_tagged,
            'text': self.text,
            'text_tagged_xml': self.text_tagged_xml,
            'target_score': self.target_score, 
            'xml': xml_text
            }
