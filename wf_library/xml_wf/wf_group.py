__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from pathlib import Path
import warnings
from typing import Any


from .const import CONTENTTYPE

from .wf import Wf
from .segment import Segment



class WfGroup:
    """
    Improved Group class allowing to access to the objects through index or object name
    
    A class container of Wf objects. It allows to work with them as a collection. They can be accessed as a dictionary or a list.
    
    Arguments:
        parent (object): Any object that might contain the
    """
    
    def __init__(self, parent: object = None, folder: str=None, recursive: bool=False):
        self.__wf_group = []
        self.__trash_bin = []
        self.__indexes = dict()
        self._folder = folder
        self._recursive = recursive
        self._index = 0
        self._last_item = len(self.__wf_group) - 1
        self._parent = parent
        if self._folder:
            self.__browse_folder()
    
    def __browse_folder(self):
        for file in Path(self._folder).glob(f"{'**/*.txlf' if self._recursive else '*.txlf'}"):
            wf= Wf(file)
            idx = len(self.__wf_group)
            wf.wf_group = self
            self.__wf_group.append(wf)
            self.__indexes[idx] = wf
    
    def __getitem__(self, key: object):
        """
        """
        if isinstance(key, int):
            if key in self.__indexes:
                return self.__wf_group[key]
            else:
                raise IndexError('Index out of range.'
                                f' It has a total of {len(self.__wf_group)} components.')
        else:
            for k, value in self.__indexes.items():
                if isinstance(key, str):
                    if key == value.wf_file:
                        return self.__wf_group[k]
                elif value == key:
                    return self.__wf_group[k]
            raise KeyError(f'The wf file with key <{key}> does not exist.')
    
    def __iter__(self):
        return iter(self.__wf_group)
    
    def __next__(self):
        if self._index < len(self.__wf_group):
            self._index += 1
            return self.__wf_group[self._index]
        else:
            raise StopIteration('Exhausted the number of WF files in this collection. '
                                        'It was the last.')
    
    def __len__(self):
        return self.__wf_group
    
    def __repr__(self):
        return f'<WFGroup {self.__wf_group}>'
    
    def __str__(self):
        return f'<WfGroup {[wf.wf_file for wf in self.__wf_group]}>'
    
    def __bool__(self):
        if len(self.__wf_group) > 0:
            return True
        else:
            return False
    
    def __contains__(self, wf: Any):
        return self.has(wf)
    
    @property
    def trash_bin(self):
        return self.__trash_bin
    
    def add(self, wf: Wf):
        if isinstance(wf, Wf):
            idx = len(self.__wf_group)
            wf.wf_group = self
            self.__wf_group.append(wf)
            self.__indexes[idx] = wf
        else:
            raise TypeError(f'It must be an Wf object. It was {type(wf)}')
    
    def append(self, wf: Wf):
        """
        """
        warnings.warn('Deprecated method. Use add instead')
        self.add(wf)
    
    def items(self):
        """
        """
        for wf in self.__wf_group:
            yield wf.wf_file, wf
    
    def delete(self, wf_object: Wf):
        """
        Remove a wf_object from the list and add it to the trash bin
        Arguments:
        wf_objects: Object file or object_files to be deleted
        """
        if isinstance(wf_object, Wf):
            if self.__has_internal(str(wf_object.wf_file_path.name)):
                self.__delete_internal(wf_object)
    
    def recover(self, wf_object: Wf):
        """
        Restore a removed Wf file from the trash bin
        """
        if isinstance(wf_object, Wf):
            if wf_object in self.trash_bin:
                self.__wf_group.append(wf_object)
                self.trash_bin.remove(wf_object)
        else:
            raise TypeError(f'TypeError: It has to be <Wf> and the type was {type(wf_object)}')
    
    def __delete_internal(self, wf_object):
        deleted = self.__wf_group[self.__wf_group.index(wf_object)]
        if deleted:
            self.__trash_bin.append(wf_object)
            #del self.__wf_group[wf_object]
            self.__wf_group.remove(wf_object)
    
    def __has_internal(self, wf_object: Any) -> Any:
        """
        Private method to check if a WF object is already in the list.
        """
        if isinstance(wf_object, Wf):
            return wf_object in self.__wf_group
        elif isinstance(wf_object, str):
            for file, wf in self.items():
                if Path(wf_object).is_absolute():
                    if str(file).lower() == str(wf_object).lower():
                        return True
                else:
                    if Path(file).name.lower() == str(wf_object).lower():
                        return True
                    elif Path(file).stem.lower() == str(wf_object).lower():
                        return True
            return False
        else:
            raise TypeError('Not recognized type to check.')
    
    def has(self, wf_object: Any):
        """
        Check if the wf_object already exist in WfGroup
        Arguments:
        wf_object: Object Wf to check if it is contained in the group or not
        """
        res_value = False
        if isinstance(wf_object, Wf):
            if self.__has_internal(str(wf_object.wf_file_path.name)):
                res_value = True
        else:
            res_value = self.__has_internal(wf_object)
        return res_value
    
    def find(self, text: str, content_type: str= CONTENTTYPE) -> Segment:
        """
        """
        for wf in self.__wf_group:
            for seg in wf:
                if content_type == 'source':
                    cont = seg.source
                else:
                    cont = seg.target
                if text == cont.text:
                    return seg
