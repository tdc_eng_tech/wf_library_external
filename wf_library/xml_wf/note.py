__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from lxml import etree as et

class Note:
    """
    Note element
    """
    def __init__(self, xml: et.Element, wf_version: str='txlf', text: str= '', parent: object=None):
        """
        """
        self._xml = xml
        self._parent = parent
        self._wf_version = wf_version
        self._text = text
        self._type = ''
        self._id = None
    
    def __repr__(self):
        return f'<Note {self.id}>'
    
    @property
    def text(self):
        """
        """
        return self._text
    
    @text.setter
    def text(self, value):
        if isinstance(value, str):
            self._text = value
            self._xml.text = value
        else:
            raise TypeError(f'The text of the note must be <str> and it is {type(value)}')
    
    @property
    def parent(self):
        """
        """
        return self._parent
    
    @property
    def xml(self):
        return self._xml
    
    @property
    def type(self):
        """
        """
        if self._xml:
            self._type = self._xml.get('segment-note-type')
        return self._type
    
    @property
    def id(self):
        if self._xml:
            self._id = self._xml.get('snid')
