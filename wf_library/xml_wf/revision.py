__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from typing import Any
from lxml import etree as et
import warnings

from .const import txlf_nsmap
from .review_info import ReviewInfo
from .segmentcontent import SegmentContent


from ..generic.gfunctions import validate_timestamp


class Revision:
    """
    Handle the revisions of a segment.
    It is a class that should not be directly instantiated.
    Use it in segment.revisions.
    Example:
        file_wf = Wf('c:\wf_file\wf_file.txlf')
        for paragraph in file_wf.paragraphs:
            for segment in paragraph.segments:
                print(len(segment.revisions)) # Show number of revisions
    """
    
    def __init__(self, xml_revision: Any=None, wf_version: str='txlf', parent: Any=None):
        """
        Initialitzer of Revision. It initialitzes the object
        when it is created/instantatied with the contents of 
        the xml of <revisions> or <alt-trans> and it inherites from its
        parent (Segment) the wf_version that is necessary to handle
        correctly the file, depending if it is TXLF or TXML.
        """
        self._xml = xml_revision
        self.wf_version = wf_version
        # Parents
        self._parent = parent
        # Other initializations
        self._add_target()
        self.__attributes = dict()
        self._notes = []
        self._reviews = []
        self._xml_notes = None
        self.__load_notes()
        self._score = None
        self._username = None
        self._phase_name = None
        self._seginfo = None
        self._state_qualifier = self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}state-qualifier') if self._xml is not None \
                                        and self.wf_version== 'txlf' else None
        if self._xml is not None:
            if self.wf_version == 'txlf':
                self._score = self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}score')
                self._phase_name = self._xml.get('phase-name')
                
                self._seginfo = self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}seginfo')
        self.load_review_info()
    
    @property
    def parent(self):
        """
        Parent element of the revision, normally the segment.
        """
        if self._parent:
            return self._parent
    
    @parent.setter
    def parent(self, value: Any):
        if not isinstance(value, (str, int, tuple)):
            self._parent = value
    
    @property
    def segment(self):
        """
        Alias of parent if the parent is a segment
        """
        return self._parent
    
    @property
    def xml(self):
        return self._xml
    
    @xml.setter
    def xml(self, value: et.Element):
        if isinstance(value, et.Element):
            self._xml = value
    
    @property
    def xml_revision(self):
        """
        """
        warnings.warn('Deprecated property. Use xml instead.')
        return self.xml
    
    @property
    def reviews(self):
        """
        Reviews of this Revision.
        """
        return self._reviews
    
    def load_review_info(self):
        """
        Read the items gs4tr:review from the segment.
        """
        if self.wf_version == 'txlf':
            if self.xml is not None:
                xml_reviews = self.xml.xpath('gs4tr:review',  namespaces=txlf_nsmap)
                for xml_review in xml_reviews:
                    self._reviews.append(ReviewInfo(xml_review, self.wf_version, self))
    
    def delete_review(self, review: ReviewInfo):
        """
        Delete a review from the revisions
        """
        rev = self.reviews.pop(self.reviews.index(review))
        rev.xml.getparent().remove(rev.xml)
    
    def delete_reviews(self):
        """
        Delete all reviews from current revision.
        """
        for rev in self.reviews:
            self.delete_review(rev)
    
    def _add_target(self):
        """
        Internal method that set the value of target.
        Add a SegmentContent object that reads the content of <target>
        in <revisions> or <alt-trans>.
        """
        self.target = SegmentContent('target', self._xml, self.wf_version)
    
    def __load_notes(self):
        if self.wf_version == 'txlf':
            self._xml_notes = self._xml.xpath('gs4tr:note', namespaces=txlf_nsmap)
        if self._xml_notes:
            self._notes = [xml_note.text for xml_note in self.xml_notes]
    
    @property
    def notes(self):
        """
        Get the notes from revisions
        """
        return self._notes
    
    @property
    def xml_notes(self):
        return self._xml_notes
    
    @property
    def attributes(self):
        """
        Get all the attributes of the revision section.
        It will return the raw attributes and not formatted.
        """
        if not self.__attributes:
            for attribute, value in self.xml_revision.attrib.items():
                self.__attributes[attribute] = value
        return self.__attributes
    
    def get_attribute(self, attribute):
        """
        Get the value of the indicated attribute.
        Arguments:
            attribute (str): Name of the attribute to retrieve.
        Return:
            value(str): The value of the indicated attribute.
            If it is empty or does not exist return None.
        """
        if not self.__attributes and self._xml is not None:
            for attr, value in self._xml.attrib.items():
                if attr == attribute:
                    return value
        elif self.__attributes:
            for attr,  value in self.__attributes.items():
                if attr == attribute:
                    return value
        elif not self.__attributes and not self._xml:
            return None
    
    def set_attribute(self, attribute, value):
        """
        Add or modidfy an existing attribute.
        Warning: If an attribute needs to be added to a TXLF file,
        it is important to know if the attribute has namespaces, 
        for example gs4tr:score.
        The namespaces for gs4tr would be {http://www.gs4tr.org/schema/xliff-ext}.
        So to add the attribute score in a TXLF file would be done as 
        the following example:
        
        Example:
            set_attribute('{http://www.gs4tr.org/schema/xliff-ext}score', '100')
        
        Arguments:
            attribute (str): Attribute to modify or add.
        """
        self.__attributes[attribute] = value
        if self._xml:
            self._xml.attrib[attribute] = value
    
    @property
    def score(self) -> int:
        """
        Score of the revision.
        """        
        if self._score:
            return self._score
        else:
            if self._xml:
                if self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}score') \
                   and self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}score').isnumeric():
                    score = int(self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}score'))
                else:
                   score = self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}score') 
                return score
    
    @score.setter
    def score(self, value: int):
        if isinstance(value, int):
            self._score = value
            self._xml.set('{http://www.gs4tr.org/schema/xliff-ext}score', str(value))
    
    @property
    def type(self) -> str:
        """
        Return the type of alt-trans. Normally "previous-version".
        Read-only property.
        """
        return self.get_attribute('alttranstype')
    
    @property
    def phase_name(self) -> str:
        """
        Phase name
        """
        return self.get_attribute('phase-name')
    
    @phase_name.setter
    def phase_name(self, value: str):
        if isinstance(value, str):
            self.set_attribute('phase-name', value)
        else:
            raise TypeError(f'The {value} is <{type(value)}> and is expected <str>.')
    
    @property
    def seginfo(self) -> str:
        """
        """
        if self._seginfo:
            return self._seginfo
    
    @property
    def seginfo_dict(self) -> dict:
        """
        Return the seginfo as a dictionary
        """
        xml_seginfo = et.fromstring(self._seginfo)
        return xml_seginfo.attrib
    
    @property
    def username(self) -> str:
        """
        Get and set the username values
        """
        if self.wf_version == 'txlf':
            if self._seginfo:
                xml_seg = et.fromstring(self._seginfo)
                return xml_seg.get('username')
        else:
            raise NotImplementedError('Username is not supported in TXML files.')
    
    @username.setter
    def username(self, value:  str):
        if self.wf_version == 'txlf':
            if self._seginfo:
                xml_seg = et.fromstring(self._seginfo)
                xml_seg.set('username', value)
                seginfo = et.tostring(xml_seg)
                self.xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', seginfo)
                self._seginfo = seginfo
        else:
            raise NotImplementedError('Username is not supported in TXML files.')
    
    @property
    def timestamp(self) -> str:
        """
        Return the timestamp of the creation of the Revision that is extracted from seginfo.
        """
        if self.wf_version == 'txlf':
            if self.seginfo_dict:
                return self.seginfo_dict.get('timestamp')
    
    @timestamp.setter
    def timestamp(self, value: str):
        if self.wf_version == 'txlf':
            if validate_timestamp(value):
                if self._seginfo:
                    xml_seg = et.fromstring(self._seginfo)
                    xml_seg.set('timestamp', value)
                    seginfo = et.tostring(xml_seg)
                    self.xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', seginfo)
                    self._seginfo = seginfo
        else:
            raise NotImplementedError('Username is not supported in TXML files.')
    
    @property
    def seginfo_set(self) -> str:
        """
        Return the timestamp of the creation of the Revision that is extracted from seginfo.
        """
        if self.wf_version == 'txlf':
            if self.seginfo_dict:
                return self.seginfo_dict.get('set')
    
    @seginfo_set.setter
    def seginfo_set(self, value: str):
        if self.wf_version == 'txlf':
            if self._seginfo:
                xml_seg = et.fromstring(self._seginfo)
                xml_seg.set('set', value)
                seginfo = et.tostring(xml_seg)
                self.xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', seginfo)
                self._seginfo = seginfo
        else:
            raise NotImplementedError('Username is not supported in TXML files.')
    
    @property
    def state_qualifier(self) -> str:
        """
        State-qualifier of the Revision
        """
        if self._state_qualifier:
            return self._state_qualifier
        else:
            if self._xml is not None:
                return self._xml.get('{http://www.gs4tr.org/schema/xliff-ext}state-qualifier')
    
    @state_qualifier.setter
    def state_qualifier(self, value: str):
        if isinstance(value, str):
            self._state_qualifer = value
            self._xml.set('{http://www.gs4tr.org/schema/xliff-ext}state-qualifier', value)
    
    def set_seginfo(self, username: str=None, timestamp: str=None,
                    tool_name: str= None, tool_version: str=None, set_revision: str=None) -> str:
        """
        Add seginfo to the file
        Args:
            username (str):
                Optional; DESCRIPTION Defaults to None.
            timestamp (str):
                Optional; DESCRIPTION Defaults to None.
            tool_name (str):
                Optional; DESCRIPTION Defaults to None.
            tool_version (str):
                Optional; DESCRIPTION Defaults to None.
            set (str):
                Optional; DESCRIPTION Defaults to None.
        
        Returns:
            None
        """
        if self.seginfo:
            root = et.fromstring(self.seginfo)
        else:
            root = et.Element('root')
        if username:
            root.set('username', username)
        if timestamp:
            if validate_timestamp(timestamp):
                root.set('timestamp', timestamp)
            else:
                raise ValueError(f'The timestamp applied is not valid: {timestamp}.'
                                 'It should follow this format: "%Y%m%dT%H%M%SZ".')
        if tool_name:
            root.set('tool-name', tool_name)
        if tool_version:
            root.set('tool-version', tool_version)
        if set_revision:
            root.set('set', set_revision)
        self._xml.set('{http://www.gs4tr.org/schema/xliff-ext}seginfo', et.tostring(root, 
                                                                                    encoding='unicode'))
        return et.tostring(root, encoding='unicode')
    
    
