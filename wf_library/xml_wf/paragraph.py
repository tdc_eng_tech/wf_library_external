__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from pathlib import Path
from lxml import etree as et
import re

from .segment import Segment
from .segmentcontent import SegmentContent
from .segments import Segments
from .const import txlf_nsmap


class Paragraph:
    """
    This class should not be directly instanced. It should be accessed from paragraphs of Wf.
    It reads the paragraphs or blocks of segments of a WordFast file.
    
    Access to the paragraph through the attribute paragraphs of the Wf object.
    
    Example:
        wf = Wf('c:\txlf_to_process\txlf_file.txlf')
        print(len(wf.paragraphs)) # Show the total of paragraphs
    
    Attributes:
        segments (list): Stores all the segments of the paragraph.
        xml_par (Element): Contains the xml of the paragraph element.
        wf_version (str): stores the WordFast version of the file (TXLF or TXML)
        wf (Wf): Reference to the Wf object that is parent of this Paragraph.
    
    """
    def __init__(self, xml_paragraph, wf_version, parent: object=None, wf: object=None):
        """
        Initialization of the paragraph.
        Arguments:
            xml_paragraph (Element): The XML element of the paragraph
            wf_version (str): WordFast version of the file.
        """
        #self._segments = []
        self._segments = Segments(self)
        self.__id = ''
        self.__user_attributes = dict()
        self.__attributes = dict()
        self._wf = wf
        self.xml_par = xml_paragraph
        self._wf_version = wf_version
        self.__append_segments()
        self.__maxlen = None
        self.__is_segmented = None
        self.__append_hidden_segments()
        self._parent = parent
        if self.wf_version == 'txlf':
            try:
                if self.xml_par.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}segmented']:
                    self.__is_segmented = True
            except KeyError:
                self.__is_segmented = False
    
    def __repr__(self):
        return f'<Paragraph {self.id}>'
    
    @property
    def segments(self):
        """
        Read-only property to access to the segments of the Paragraph.
        """
        return self._segments
    
    @property
    def wf_version(self) -> str:
        """Type of Wf file (TXLF/TXML)"""
        return self._wf_version
    
    @property
    def wf(self) -> str:
        return self._wf
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def xml(self) -> et.Element:
        """
        Shows and updates the xml of the Paragraph Element.
        """
        return self.xml_par
    
    @xml.setter
    def xml(self, value) -> None:
        if isinstance(value, et.Element):
            self.xml_par = value
        else:
            raise TypeError('Wrong type, it is not an etree.Element as expected.')
    
    @property
    def previous(self):
        """
        Return the previous paragraph
        """
        if self.wf.paragraphs.index(self) > 0:
            return self.wf.paragraphs[self.wf.paragraphs.index(self) - 1]
        else:
            raise StopIteration('It was the first paragraph, there are no more.')
    
    @property
    def next(self):
        """
        Return the next paragraph
        """
        if self.wf.paragraphs.index(self) < len(self.wf.paragraphs) - 1:
            return self.wf.paragraphs[self.wf.paragraphs.index(self) + 1]
        else:
            raise StopIteration('It was the last paragraph, there are no more.')
    
    def __append_segments(self):
        """
        Internal method to add the segments to the segments list.
        """
        if self.wf_version == 'txlf':
            xml_segments = self.xml_par.xpath('Default:trans-unit',
                 namespaces=txlf_nsmap)
        else:
            xml_segments = self.xml_par.xpath('./segment')
        for idx, xml_segment in enumerate(xml_segments):
            self._segments.add(Segment(xml_segment, self.wf_version,
                                        par_position=idx + 1, parent=self.segments, paragraph=self, wf=self.wf))
#        self._segments = [Segment(xml_segment, self.wf_version, par_position=idx + 1, parent=self
#                                 ) for idx, xml_segment in enumerate(xml_segments)]
    
    def __append_hidden_segments(self):
        """
        Internal method to find hidden segments in TXML
        """
        if self.wf_version == 'txml':
            xml_hidden = self.xml_par.xpath('comment()')
            for comment in xml_hidden:
                xml_segment = et.XML(comment.text)
                segment = Segment(xml_segment, self.wf_version, hidden=True, parent=self)
                segment.xml_comment = comment
                self._segments.add(segment)
    
    @property
    def id(self):
        """
        ID of the paragraph. It is a read only property.
        """
        if self.__id:
            return self.__id
        else:
            if self.wf_version == 'txlf':
                self.__id = self.xml_par.attrib['id']
            else:
                self.__id = self.xml_par.attrib['blockId']        
            return self.__id
    
    @property
    def user_attributes(self):
        """
        Return the custom properties of the existing WF file. Custom attributes if it
        is TXLF or satt_something if it is txml.
        """
        key_value_pat_txlf_str = r'(?P<key>\w+?)="(?P<value>.*?)"'
        key_value_txlf_pat = re.compile(key_value_pat_txlf_str)
        if not self.__user_attributes:
            if self.wf_version == 'txlf':
                try:
                    user_attributes = self.xml_par.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}user-attributes']
                except KeyError:
                    user_attributes = dict()
                if user_attributes:
                    key_value_list = key_value_txlf_pat.findall(user_attributes)
                    self.__user_attributes = {key:value for key, value in key_value_list}
            else:
                try:
                    for attribute, value in self.xml_par.attrib.items():
                        if str(attribute).startswith('satt'):
                            self.__user_attributes[attribute] = value
                except KeyError:
                    self.__user_attributes = dict()
                    return self.__user_attributes
        return self.__user_attributes
    
    @property
    def full_paragraph_only_text(self):
        """
        Special property that shows all the text of the paragraph segments together.
        It only reads the source text.
        Read-only property.
        Return:
        full_text (str): Joined text contained in all segments.
        """
        full_text = ''
        
        for segment in self._segments:
            try:
                full_text += segment.source.text
            except TypeError:
                pass
                #TODO: Add a logging exception here.
            if self.wf_version == 'txlf':
                ws = segment.xml_seg.xpath('gs4tr:ws', namespaces=txlf_nsmap)
                if ws:
                    try:
                        if ws[0].attrib['{http://www.gs4tr.org/schema/xliff-ext}pos'] == 'after':
                            full_text += segment.source.text_only + ws[0].text
                    except KeyError:
                        pass
        return full_text
    
    @property
    def full_paragraph_src_all_text(self):
        """
        Special property that shows all the text of the paragraph. Including the text 
        between tags.
        Read only property
        Return:
        all_text (str): Joined text contained in all segments.
        """
        all_text = ''
        for segment in self._segments:
            all_text += segment.source.text
        return all_text

    @property
    def full_all_source_text(self):
        """
        Special properties to get all the source text included trailing spaces and text between tags.
        Read only property
        Return:
            all_text (str):
        """
        all_text = ''
        for segment in self._segments:
            all_text += segment.source.text_ws
        return all_text

    @property
    def full_all_target_text(self):
        """
        Special properties to get all the target text included trailing spaces and text between tags.
        Read only property
        Return:
            all_text (str):
        """
        all_text = ''
        for segment in self._segments:
            all_text += segment.target.full_all_text
        return all_text

    @property
    def is_segmented(self):
        """
        Option only available for TXLF files.
        """
        return self.__is_segmented

    def enable_segmented(self, value: bool=True) -> int:
        """
        Enable or disable the segment as segmented.
        
        Arguments:
            value (bool): True to enable 
        """
        if self.wf_version == 'txlf':
            self.__is_segmented = value
            if self.__is_segmented:
                self.xml_par.attrib['{http://www.gs4tr.org/schema/xliff-ext}segmented'] = 'true'
            else:
                try:
                    del self.xml_par.attrib['{http://www.gs4tr.org/schema/xliff-ext}segmented']
                except KeyError:
                    pass
            return 0
        else:
            return -1
    
    @property
    def hidden_segments(self):
        """
        Hidden segments
        """
        for segment in self.hidden_segments:
            return self.__hidden_segments

    def resegment_tag(self, text: str='softReturn'):
        """
        Resegment a paragraph based on the contents of a tag.
        By default it assumes that it contains 'softReturn' value to indicate
        it is a line break. So, it segments based on line break.
        
        Arguments:
            text (str): Value that has to be looked for in the tags.
        """
        print(self.segment_count)
        if self.segment_count == 1:
            seg_temp = self._segments[0]
            
            tags_count_src = seg_temp.source.get_number_tags(text)
            print(tags_count_src)
            id = 1
            if seg_temp.target:
                tags_count_tgt = seg_temp.target.get_number_tags(text)
                print(tags_count_tgt)
                if tags_count_src == tags_count_tgt:
                    src_tags = [tag for tag in self.__divide_segment(seg_temp.source, text)]
                    tgt_tags = [tag for tag in self.__divide_segment(seg_temp.target, text, 'target')]
                    print(src_tags)
                
            if seg_temp.source:
                print(tags_count_src)
                src_tags = [tag for tag in self.__divide_segment(seg_temp.source, text)]

    def __divide_segment(self, seg_cont: SegmentContent, text: str='softReturn', name='source'):
        """
        Divide a segment, not totally implemented yet.
        """
        txt = seg_cont._xml.text
        new_seg = None
        is_new = False
        print(seg_cont._xml.text)
        id = 1
        text_added = False
        for tag in seg_cont._xml.getchildren():
            if is_new:
                new_seg = None
                id = 1
            if text not in tag.text:
                is_new = False
                if new_seg is not None:
                    new_seg += tag
                else:
                    if self.wf_version == 'txml':
                        new_seg = et.Element('segment')
                        new_seg.attrib['segmentId'] = id
                        new_content = et.SubElement(new_seg, name)
                        if not text_added:
                            new_content.text = txt
                            text_added = True
            else:
                is_new = True
                print(txt, new_seg, tag)
                yield txt, new_seg, tag

    def add_user_attribute(self, key, value):
        """
        Add the user attributes to the paragraph.
        The user attributes for the old Wf version (TXML) are added with prefix satt_ and for the new format
        are added as a value for the attribute user-attributes.
        
        Example:
        TXML
        satt_note="Something"
        
        TXLF:
        user-attributes= "&lt;attr note=&quot;Something&quot;&gt;"
        
        Arguments:
            key: Name of the attribute to be added.
        """
        #pdb.set_trace()
        self.__user_attributes[key] = value      
        if self.wf.wf_file_path.is_file():                     
            if self.wf_version == 'txlf':
                try:
                    user_attributes = self.xml_par.attrib[
                                '{http://www.gs4tr.org/schema/xliff-ext}user-attributes']                  
                    attrib_xml = et.fromstring(user_attributes)
                except KeyError:
                    attrib_xml = et.Element('attrib')
                attrib_xml.attrib[key] = value    
                self.xml_par.attrib[
                    '{http://www.gs4tr.org/schema/xliff-ext}user-attributes'] = et.tostring(attrib_xml)
            else:
                self.xml_par.attrib[key] = value
    
    @property
    def attributes(self):
        """
        Get all the attributes of the paragraph.
        It will return the raw attributes and not formatted.
        """
        if not self.__attributes:
            for attribute, value in self.xml_par.attrib.items():
                self.__attributes[attribute] = value
        return self.__attributes
       
    @property
    def maxlen(self):
        """
        Handles the maxlen of a paragraph. Get and set the maxlen of a paragraph.
        """
        if not self.__maxlen and Path(self.wf.wf_file).is_file():
            try:
                if self.wf_version == 'txlf':
                    self.__maxlen = self.xml_par.attrib[
                             '{http://www.gs4tr.org/schema/xliff-ext}maxlen']
                else:
                    self.__maxlen = self.xml_par.attrib['maxlen']
            except KeyError:
                self.__maxlen = None
        return self.__maxlen
        
    @maxlen.setter
    def maxlen(self, value: int):
        self.__maxlen = str(value)
        if Path(self.wf.wf_file).is_file():
            if self.wf_version == 'txlf':
                if self.__maxlen:
                    self.xml_par.attrib[
                                 '{http://www.gs4tr.org/schema/xliff-ext}maxlen'
                                 ] = self.__maxlen
                else:
                    try:
                        self.xml_par.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}maxlen')
                    except KeyError:
                        pass
            else:
                if self.__maxlen:
                    self.xml_par.attrib['maxlen'] = self.__maxlen
                else:
                    try:
                        self.xml_par.attrib.pop('maxlen')
                    except KeyError:
                        pass
    
    def merge_segments(self):
        """
        Merge the segments in the paragraph into 1 segment.
        And deletes the rest of the segments from the object Paragraph and XML
        """
        if self.segment_count > 1:
            for idx, seg in enumerate(self._segments):
                if idx > 0:
                    self._segments[0].source.paste_segment_content(seg.source, False)
                    if self._segments[0].target and seg.target:
                        print(self._segments[0].target, seg.target)
                        self._segments[0].target.paste_segment_content(seg.target, False)
                    seg.xml.getparent().remove(seg.xml)
            del self._segments[1:]
    
    @property
    def segment_count(self) -> int:
        """
        Return the number of segments in the paragraph.
        """
        return len(self._segments)

    def jsonify(self):
        """
        Return the dictionary serialized object to convert to JSON for API purposes.
        """
        return {
            'id': self.id,
            'is_segmented': self.is_segmented,
            'maxlen': self.maxlen, 
            'user_attributes': self.user_attributes, 
            'attributes': self.attributes,
            'xml': et.tostring(self.xml_par, encoding=str),
            'wf_version': self.wf_version, 
            'segments': [segment.jsonify() for segment in self._segments]
            }
