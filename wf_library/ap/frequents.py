__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

import logging
import platform
from pathlib import Path
from subprocess import run

from .const import _no_console

from ..xml_wf.wf_group import WfGroup
from ..xml_wf.wf import Wf



def extract_frequents(files_path_str: str, unk_dir_str: str,  ap_path_str: str, ap_version: str,
                        src_lang: str, tgt_lang: str, threaded: bool=False, num_segments: str='1000',
                        include_hundred: bool=False, 
                        one_file: bool=False, tm='', debug: bool=False, capture_output: bool=True,
                        basename: str=''):
    """
    Create frequent files from the indicated path. Folder or file. It generates
    a unique frequent file.
    run:
        create_frequents(files_path_str, unk_dir_str, ap_pat_str,
                            src_lang, tgt_lang, threaded=False) return result
    Arguments:
    files_path_str: Path to the files to process to generate the UNK files.
    unk_dir_str: String path to the files where store the UNK files.
    ap_path_str: Path to AP version to run the command extractfrequents.
    ap_version: TXLF or TXML as values.
    src_lang: Source language of the frequents.
    tgt_lang: Target language of the frequents..
    threaded: Indicate if the process needs to be run in a threaded mode.
    num_segments (str): Indicate the number of segments that each UNK file should contain.
    include_hundred (bool): If the 100% matches should be included (False by default)
    one_file (bool): Indicate if the UNK should be one file or not.
    basename (str): Indicate the basename of the UNK files.
    
    Return:
        If success:
            frequents_wf (WfGroup): Return a WfGroup object containing all the frequents for this lang-pair.
        If failure:
            -1: If the folder to store the UNK files is not valid.
            -2: If the AP is not valid or does not exist.
            -3: UNK files don't exist
    """
    os_version = platform.system()
    if debug:
            cmd_creationflags = 0
    else:
        cmd_creationflags = _no_console
    if os_version == 'Windows':
        command = Path(ap_path_str, 'extractfrequents.cmd')
    else:
        command = Path(ap_path_str, 'extractfrequents.ksh')
    command_str = str(command)
    files_path = Path(files_path_str)
    unk_dir = Path(unk_dir_str)
    tgt_lang = tgt_lang.replace('_', '-')
    if not unk_dir.exists():
        try:
            unk_dir.mkdir(parents=True, exist_ok=True)
        except OSError:
            logging.debug('create_frequents: Invalid folder to store UNK: {}'.format(
                    unk_dir_str))
            return -1
        except FileExistsError:
            pass
    if Path(ap_path_str).exists():
        
        ap_exists = True
    else:
        logging.critical('create_frequents: AP path does not exist {}'.format(
                        ap_path_str))
        ap_exists = False
        return -2
    if ap_exists and Path(ap_path_str).exists():
        files_to_process = files_path_str
        if files_path.exists() and not threaded and ap_version.upper() == 'txml'.upper():
            full_command = [command_str,
                        str(files_to_process),
                        '-n', num_segments, 
                        '-s', src_lang, 
                        '-o', tgt_lang, 
                        '-B', str( unk_dir_str), 
                        '-m', '1', 
                        '-b', tgt_lang + '-']
            logging.debug('create_frequents: The command is: {}'.format(
                        full_command))
            #run(full_command, shell=True)
        elif files_path.exists() and not threaded and ap_version.upper() == 'txlf'.upper():
            full_command = [command_str,
                        str(files_to_process),
                        '-n', num_segments, 
                        '-l', src_lang, 
                        '-p', tgt_lang, 
                        '-m', '1']
            full_command.append('-b')
            if basename:
                full_command.append(basename)
            else:
                full_command.append(tgt_lang + '-')
        if include_hundred:
            full_command.append('-i')
        if one_file:
            full_command.append('-1')
        if tm:
            full_command.append('-t')
            full_command.append(tm)
        print(full_command)
        if ap_version.upper() == 'txml'.upper():
            res = run(full_command, shell=True, capture_output=capture_output, creationflags=cmd_creationflags)
            print(full_command, res)
        else:
            res = run(full_command, shell=False, capture_output=capture_output, creationflags=cmd_creationflags, cwd=str(unk_dir))
    unk_files = list(Path(unk_dir_str).glob('*.' + ap_version))
    frequents_wf = WfGroup()
    if len(unk_files) > 0:
        for file in unk_files:
            frequents_wf.append(Wf(str(file)))
        return frequents_wf
    else:
        return -3
