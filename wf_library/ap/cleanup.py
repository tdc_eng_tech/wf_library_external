__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from pathlib import Path
import platform
from subprocess import run, Popen

from .const import _no_console

from typing import Literal

MERGETYPE = Literal['doc', 'ppt', 'visio', 'idml', 'xliff', 'xls', 'reg', 'json', 'illustarator', 'resx', 'srt',
                    'quark', 'txml', 'tmx']

class Cleanup:
    """
    Cleanup the files to get the translated files
    """
    excel_file_ext = ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm')
    doc_file_ext = ('.doc', '.docx', '.docm', '.dot',
                      '.dotm', '.rtf')
    xml_file_ext = ('.xml', '.dita', '.tag')
    ppt_file_ext = ('.ppt', '.pptx', '.pptm')
    xlf_file_ext = ('.xlf', '.xliff', '.sdlxliff')
    txt_file_ext = ('.json', 'yml', '.txt')
    html_file_ext = ('.html', '.htm', '.chm')
    visio_file_ext = ('.vsd', '.vdx', '.vsdx')
    office_files_ext =('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm',
                            '.doc', '.docx', '.docm', '.dot', '.dotm', '.rtf', 
                            '.ppt', '.pptx', '.pptm')
    
    def __init__(self, ap_path: str=r'c:\Program Files (x86)\Analysis Package\AP4', ap_version: str='txlf',
                 debug: bool=False):
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.os = platform.system()
        self._debug = debug
    
    @property
    def debug(self):
        return self._debug
    
    @debug.setter
    def debug(self, value: bool):
        if isinstance(value, bool):
            self._debug = value
        else:
            raise TypeError(f'Wrong type,  expected <bool> and got {type(value)}')

    def merge(self, wf_file, type_file: MERGETYPE, source_file: str = '', threaded=False, capture_output: bool = True,
              overwrite_source: bool = False):
        """
        Method for the merge of different formats like Excel, Word,...
        Arguments:
            wf_file: WordFast file
            type_file (MERGETYPE): Literal that can be: 'doc', 'ppt', 'visio', 'idml', 'xliff', 'xls'
            source_file (str ,optional): Compatibility with the old .txml type
            threaded (bool, optional): if the execution should be threaded or not.
            overwrite_source (bool, optional): if source files should be overwritten instead of having the lang
            extension added, only works for XML files

        Return:
            None: If the operation was successful.
            err (str): Reason of the error if there was any error.
        """
        source_file_path = Path(source_file)
        source_file_str = str(source_file_path)
        wf_file_path = Path(wf_file)
        wf_file_str = str(wf_file_path)
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        #        if type_file in ('xls', 'doc', 'ppt', 'idml', 'xliff', 'reg'):
        #            merge_suffix = type_file.upper()
        if type_file == 'visio' or type_file == 'resx' or type_file == 'illustrator' or type_file == 'quark':
            merge_suffix = type_file.capitalize()
        else:
            merge_suffix = type_file.upper()
        if source_file:
            if source_file_path.suffix in self.office_files_ext:
                if source_file_path.suffix in self.excel_file_ext:
                    merge_suffix = 'XLS'
                if source_file_path.suffix in self.doc_file_ext:
                    merge_suffix = 'DOC'
                if source_file_path.suffix in self.ppt_file_ext:
                    merge_suffix = 'PPT'
        if self.ap_path.exists():
            if self.os == 'Windows':
                command = Path(self.ap_bin_path, f'merge{merge_suffix}.cmd')
            else:
                command = Path(self.ap_bin_path, f'merge{merge_suffix}.ksh')
            command_str = str(command)
            if not source_file:
                full_command = [command_str, wf_file_str]
            elif source_file and source_file_path.is_file() and wf_file_path.is_file():
                full_command = [command_str, source_file_str, wf_file_str]
            else:
                return -2
            if overwrite_source:
                full_command.append("-a")
            if not threaded:
                if self.os == 'Windows':
                    res = run(full_command, shell=False, capture_output=capture_output,
                          creationflags=cmd_creationflags, cwd=str(wf_file_path.parent))
                else:
                    res = run(full_command, shell=False, capture_output=capture_output,
                          cwd=str(wf_file_path.parent))
                if capture_output:
                    err = res.stderr.decode('utf-8')

            else:
                if self.os == 'Windows':
                    process = Popen(full_command, shell=False, creationflags=cmd_creationflags,
                                    cwd=str(wf_file_path.parent))
                else:
                    process = Popen(full_command, shell=False,
                                    cwd=str(wf_file_path.parent))
                err = process.communicate()[1]
            if err:
                if overwrite_source and type_file == 'reg' and "FileExistsException" in err:
                    # removes source XML files from the directory if the merge with overwrite failed
                    if Path(wf_file_str).is_dir():
                        xml_files = Path(wf_file_str).glob('**/*xml')  # recursively
                        for xml in xml_files:
                            xml.unlink()
                    elif Path(wf_file_str).is_file():
                        xml_file = Path(Path(wf_file_str).parent, Path(wf_file_str).stem)
                        xml_file.unlink()
                    self.merge(wf_file, type_file, source_file, threaded, capture_output, overwrite_source)
                if 'failed' in err or 'ignoring' in err:
                    return err
            return
        else:
            return 'AP not found'
