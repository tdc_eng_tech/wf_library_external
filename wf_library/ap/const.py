__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

_no_console = 0x08000000

AP2_BIN_LOCAL = r'c:\Program Files (x86)\Analysis Package\bin'
AP4_BIN_LOCAL = r'c:\Program Files (x86)\Analysis Package\AP4\bin'
AP2_BIN_REMOTE = r'\\bcnal-share\public\production\engineering\Admin_Engineering\_Scripts\_Controlled_Versions\AP2.20\bin'
AP4_BIN_REMOTE = r'\\bcnal-share\public\production\engineering\Admin_Engineering\_Scripts\_Controlled_Versions\AP4.18\bin'
AP5_BIN_REMOTE = r'\\bcnal-share\public\production\engineering\Admin_Engineering\_Scripts\_Controlled_Versions\AP5_2018_04\bin'
