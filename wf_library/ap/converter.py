__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from pathlib import Path
import platform
from subprocess import run, Popen
from typing import Any


from .commands import _add_wf_files_to_group, subprocess_args
from .const import _no_console


class Converter:
    """
    Converter class to convert any kind of file as most transparent way possible.
    
    Class Properties:
        excel_file_ext (tuple): Tuple of accepted extensions for Excel files.
        doc_file_ext (tuple): Tuple of accepted extensions for Word files.
        xml_file_ext (tuple): Tuple of accepted extensions for xml files.
        ppt_file_ext (tuple): Tuple of accepted extensions for PPT files.
        xlf_file_ext (tuple): Tuple of accepted extensionf for XLIFF format files.
        txt_file_ext (tuple): Accepted extensions for generic text files (json, yml and txt)
    
    Properties:
        ap_version (str): AP version that needs to be loaded (txlf, txml)
        src_lang (str): ISO code of the source language. Default: en
        tgt_lang (str): ISO code of the target language. Default: de
        ap_path (Path): Path to the ap commands.
        os (str): Indicate in what os is running the commands. It can be 'Linux', 'Windows' or 'MacOS'.
    """
    excel_file_ext = ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm')
    doc_file_ext = ('.doc', '.docx', '.docm', '.dot',
                      '.dotm', '.rtf')
    xml_file_ext = ('.xml', '.dita', '.tag')
    ppt_file_ext = ('.ppt', '.pptx', '.pptm')
    xlf_file_ext = ('.xlf', '.xliff', '.sdlxliff')
    txt_file_ext = ('.json', 'yml', '.txt')
    html_file_ext = ('.html', '.htm', '.chm')
    office_files_ext = ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm',
                            '.doc', '.docx', '.docm', '.dot', '.dotm', '.rtf', 
                            '.ppt', '.pptx', '.pptm', 
                            '.vdx', '.vsd', '.vsdx')
    idml_ext = ('.idml')
    
    def __init__(self, ap_path, ap_version: str='txlf', src_lang='en-US', tgt_lang='de-DE', debug: bool=False):
        self._ap_version = ap_version
        self._src_lang = src_lang
        self._tgt_lang = tgt_lang
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path_str, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.os = platform.system()
        self._debug = debug
    
    @property
    def debug(self):
        return self._debug
    
    @debug.setter
    def debug(self, value: bool):
        if isinstance(value, bool):
            self._debug = value
        else:
            raise TypeError(f'Wrong type,  expected <bool> and got {type(value)}')
    
    @property
    def ap_version(self):
        """
        """
        return self._ap_version
    
    @property
    def apversion(self):
        """
        Alias of ap_version
        """
        return self.ap_version
    
    @property
    def source_lang(self) -> str:
        """
        """
        return self._src_lang
     
    @source_lang.setter
    def source_lang(self, value: str):
        if isinstance(value, str):
            self._src_lang = value
        else:
            raise TypeError(f'The type of {value} is {type(value)} not <str>. ')
    
    @property
    def target_lang(self) -> str:
        """
        """
        return self._tgt_lang
     
    @target_lang.setter
    def target_lang(self, value: str):
        if isinstance(value, str):
            self._tgt_lang = value
        else:
            raise TypeError(f'The type of {value} is {type(value)} not <str>. ')
    
    def __check_ap(self):
        if not self.ap_version:
            raise ValueError('AP version have not been indicated')
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        return ap_exists
    
    def convert_pdf(self, files_path_str: str, lang_iso: str, threaded: bool=False):
        """
        Convert PDF files into TXML/TXLF files.
        
        Arguments:
            files_path_str (str): Path to the files to convert
            lang_iso (str): Source language of the files.
            threaded (bool): Indicate if the executions of the command has to be threaded or not.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertPDF.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertPDF.ksh')
        command_str = str(command)
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        #doc_types = ('.doc', '.docx', '.docm', '.dot', '.dotm')
        files_str = str(Path(files_path_str))
        if not self.ap_version:
            return -1
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return -1
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', lang_iso] 
            command_run = Popen(full_command, creationflags=cmd_creationflags)
            command_run.wait()
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)

    def convert_doc(self, files_path_str: str, lang_iso: str, threaded: bool=False, accept_tc: bool=True) -> Any:
        """
        Convert Word files into TXML/TXLF files.
        
        Arguments:
            files_path_str (str): Path to the files to convert
            lang_iso (str): Source language of the files.
            threaded (bool): Indicate if the executions of the command has to be threaded or not.
            accept_tc(bool): Enable flag to accept track changes, by default is enabled.
        
        Return:
            If success return a list of WF files.
                WfGroup: Group of WF files.
            If there was an error returns
                error (str): Message error.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertDOC.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertDOC.ksh')
        command_str = str(command)
        #doc_types = ('.doc', '.docx', '.docm', '.dot', '.dotm')
        files_str = str(Path(files_path_str))
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if not self.ap_version:
            return 'Error: ap version not specified'
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return 'Error: AP not found'
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', lang_iso] 
            if accept_tc:
                full_command.append('-a')
            if self.os == 'Windows':
                try:
                    res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                except OSError:
                    try:
                        res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                    except OSError as e:
                        Path('errors.text').write_text(f'Error: {e}')
                    
            else:
                res = run(full_command, capture_output=True)
            err = res.stderr.decode('utf-8')
            if err:
                if not 'Done' in err:
                    return res.stderr
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)
    
    def convert_idml(self, files_path_str: str, config: str='', threaded: bool=False, line_break_internal: bool=False) -> Any:
        """
        Convert IDML files into TXML/TXLF files.
        
        Arguments:
            files_path_str (str): Path to the files to convert
            lang_iso (str): Source language of the files.
            threaded (bool): Indicate if the executions of the command has to be threaded or not.
            line_break_internal (bool): Enable flag to add <br> as inline tag. Default is external
        
        Return:
            If success return a list of WF files.
                WfGroup: Group of WF files.
            If there was an error returns
                error (str): Message error.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertIDML.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertIDML.ksh')
        command_str = str(command)
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        files_str = str(Path(files_path_str))
        if not self.ap_version:
            return 'Error: ap version not specified'
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return 'Error: AP not found'
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', self.source_lang] 
            if line_break_internal:
                full_command.append('-b')
            if config:
                config_path = Path(config)
                if config_path.exists() and config_path.is_file():
                    full_command.extend(['-c', config])
                else:
                    raise FileNotFoundError(f'The config file{config} was not found. Command cannot be run.')
            if self.os == 'Windows':
                try:
                    res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                except OSError:
                    try:
                        res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                    except OSError as e:
                        Path('errors.text').write_text(f'Error: {e}')
                    
            else:
                res = run(full_command, capture_output=True)
            err = res.stderr.decode('utf-8')
            if err:
                if not 'Done' in err:
                    return res.stderr
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)
    
    def convert_ppt(self, files_path_str, config: str='', num_paragraphs: int=None, 
                    ignore: bool=False, threaded: bool=False) -> Any:
        """
        Convert PowerPoint files through convertppt AP command.
        
        Arguments:
            files_path_str (str): Path to the files or folder to convert
            threaded (bool): In dicate if the command needs to be run as trhreaded or not.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertPPT.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertPPT.ksh')
        command_str = str(command)
        files_str = str(Path(files_path_str))
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if not self.ap_version:
            return 'Error: ap version not specified'
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return 'Error: AP not found'
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', self.source_lang] 
            if config:
                config_path = Path(config)
                if config_path.exists() and config_path.is_file():
                    full_command.extend(['-c', config])
                else:
                    raise FileNotFoundError(f'The config file{config} was not found. Command cannot be run.')
            if num_paragraphs:
                if isinstance(num_paragraphs, int):
                    full_command.extend(['-p', str(num_paragraphs)])
            if ignore:
                full_command.append('-i')
            if self.os == 'Windows':
                try:
                    res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                except OSError:
                    try:
                        res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                    except OSError as e:
                        Path('errors.text').write_text(f'Error: {e}')
                    
            else:
                res = run(full_command, capture_output=True)
            err = res.stderr.decode('utf-8')
            if err:
                if not 'Done' in err:
                    return res.stderr
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)
        
    def convert_visio(self, files_path_str: str, threaded: bool=False) -> Any:
        """
        Convert Visio files into TXML/TXLF files.
        
        Arguments:
            files_path_str (str): Path to the files to convert
            lang_iso (str): Source language of the files.
            threaded (bool): Indicate if the executions of the command has to be threaded or not.
        
        Return:
            If success return a list of WF files.
                WfGroup: Group of WF files.
            If there was an error returns
                error (str): Message error.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertVisio.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertVisio.ksh')
        command_str = str(command)
        files_str = str(Path(files_path_str))
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if not self.ap_version:
            return 'Error: ap version not specified'
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return 'Error: AP not found'
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', self.source_lang] 
            if self.os == 'Windows':
                try:
                    res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                except OSError:
                    try:
                        res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                    except OSError as e:
                        Path('errors.text').write_text(f'Error: {e}')
                    
            else:
                res = run(full_command, capture_output=True)
            err = res.stderr.decode('utf-8')
            if err:
                if not 'Done' in err:
                    return res.stderr
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)
    
    def convert_excel(self, 
                        files_path_str,
                        excel_config_path,
                        lang_iso,
                        threaded=False, 
                        show_console=False) -> Any:
        """
        Convert a file or folder to txml or txlf.
        convert_excel_to_wf (parameters) return error_code
        
        Arguments:
        files_path_str: String that indicates the file or folder to be converted.
                         It needs to be an Excel file.
        excel_config_path: XML Configuration file for the conversion of the actual file or files.
        lang_iso: source language of the file 
        ap_path_str: Path to the AP files of the indicated version
        threaded: It allows to run the process in a threaded way, and being in parallel or not.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertxls.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertXLS.ksh')
        command_str = str(command)
        files_str = str(Path(files_path_str))
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if not self.ap_version:
            return 'Error: ap version not specified'
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return 'Error: AP not found'
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', lang_iso]
            if excel_config_path:
                full_command.extend(['-s', excel_config_path])
            if self.os == 'Windows':
                if not show_console:
                    try:
                        res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                    except OSError:
                        try:
                            res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                        except OSError as e:
                            Path('errors.text').write_text(f'Error: {e}')
                else:
                    try:
                        res = run(full_command, capture_output=True)
                    except OSError:
                        try:
                            res = run(full_command, **subprocess_args(True))
                        except OSError as e:
                            Path('errors.text').write_text(f'Error: {e}')
                    
            else:
                res = run(full_command, capture_output=True)
            err = res.stderr.decode('utf-8')
            if err:
                if not 'Done' in err:
                    return res.stderr
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)
   
    def convert(self, files_path_str: str, rules: str=None, extension: str='xml', 
                    show_console: bool=False):
        """
        Generic convert for REGEX convert
        """
        files_path = Path(files_path_str)
        files_str = str(files_path)
        rules_path = Path(rules)
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if not rules_path.exists():
            raise ValueError('Invalid rules path. The rules cannot be found.')
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convert.cmd')
        else:
            command = Path(self.ap_bin_str, 'conver.ksh')
        command_str = str(command)
        if self.__check_ap():
            full_command = [command_str, str(files_str),
                                 '-l', self.source_lang,
                                 '-x', extension, 
                                 '-r', rules]
            if self.os == 'Windows':
                if not show_console:
                    try:
                        res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                    except OSError:
                        try:
                            res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                        except OSError as e:
                            Path('errors.text').write_text(f'Error: {e}')
                else:
                    try:
                        res = run(full_command, capture_output=True)
                    except OSError:
                        try:
                            res = run(full_command, **subprocess_args(True))
                        except OSError as e:
                            Path('errors.text').write_text(f'Error: {e}')
            else:
                res = run(full_command, capture_output=True)
            err = res.stderr.decode('utf-8')
            if err:
                if not 'Done' in err:
                    return res.stderr
            return _add_wf_files_to_group(files_path_str, self.ap_version)
    
    def convert_tmx(self, files: str) -> str:
        """
        Convert TMX files into TXLF
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'converttmx.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertTMX.ksh')
        command_str = str(command)
        if not self.ap_bin_path.exists():
            return -1
        files_path = Path(files)
        full_command = [command_str, str(files_path)]
        res = run(full_command, capture_output=True)
        return res
