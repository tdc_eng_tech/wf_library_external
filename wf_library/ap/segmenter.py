__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from pathlib import Path
import platform
from subprocess import run, Popen
import logging


from .const import _no_console
from .commands import _add_wf_files_to_group, subprocess_args


class Segmenter:
    """
    Generate a Segmenter class with all the properties and methods required to segment a file or files.
    
    Attributes:
        config_path (Path): Path to the config_path used for segmenter purposes.
        ap_version (str): AP version (TXLF or TXML)
        ap_path (Path): Path to the ap commands.
        ap_bin_path (Path): Path to the bin where are the commands.
        os (str): Version of the platform/os: It can be 'Linux', 'Windows' or 'MacOS'
    """
    def __init__(self, config=None, ap_version='txlf', ap_path=r'c:\Program Files (x86)\Analysis Package\AP4', 
                 debug: bool=False):
        self.config_path = None
        self.config_str = None
        if config:
            if Path(config).exists() and Path(config).is_file():
                self.config_path = Path(config)
                self.config_str = str(self.config_path)
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.os = platform.system()
        self._debug = debug
    
    @property
    def debug(self):
        return self._debug
    
    @debug.setter
    def debug(self, value: bool):
        if isinstance(value, bool):
            self._debug = value
        else:
            raise TypeError(f'Wrong type,  expected <bool> and got {type(value)}')
    
    def segment(self, files_path_str: str, paragraph: bool=False, threaded: bool=False):
        """
        Segments the txml file with a config file to generate the comments/notes.
        Run the command segmentTXML of the indicated AP. 
        It only supports segmentTXML. SegmentTXLF is not implemented.
        Run:
        segment_wf(files_path_str, config_path_str, 
                    ap_path_str, threaded=False) return ErrorCode
        
        Arguments:
        files_path_str: path to the files to segment. 
        threaded (bool): If it is run in threaded way
        no_console (bool): Prevent to be shown the console if is selected
        
        Return:
        -1: AP does not exist
        """
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if self.ap_version == 'txlf': com_name = 'Txlf'
        else: com_name = 'Txml'
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'segment{}.cmd'.format(com_name))
        else:
            command = Path(self.ap_bin_str, 'segment{}.ksh'.format(com_name))
        command_str = str(command)
        path_files = Path(files_path_str)
        if Path(self.ap_path_str).exists():
            ap_exists = True
        else:
            logging.critical('segment_wf: AP path does not exist {}'.format(
                            self.ap_path_str))
            ap_exists = False
            return -1
        
        if ap_exists  and path_files.exists():
            files_to_process=''
            if not threaded:
                files_to_process = files_path_str
                if self.config_path:
                    full_command = [command_str,
                                '-c',
                                str(self.config_path_str),
                                str(files_to_process),
                                '-f']
                else:
                    full_command = [command_str,
                                str(files_to_process),
                                '-f']
                if paragraph:
                    full_command.append('-p')
                #print(f'Segment command: {full_command}')
                if self.os == 'Windows':
                    try:
                        res = run(full_command, capture_output=True, creationflags=cmd_creationflags)
                    except OSError:
                        try:
                            res = run(full_command, creationflags=cmd_creationflags, **subprocess_args(True))
                        except OSError as e:
                            Path('errors.text').write_text(f'Error: {e}')
                else:
                    res = run(full_command, capture_output=True)
                err = res.stderr.decode('utf-8')
                if err:
                    if not 'Done' in err:
                        return res.stderr
            if path_files.is_dir() and threaded:
                for file in path_files.glob('*.{}'.format(self.ap_version)):
                    files_to_process = str(file)
                    if self.config_path:
                        full_command = [
                                        command_str,
                                        '-c',
                                        str(self.config_path_str),
                                        str(files_to_process),
                                        '-f']
                    else:
                        full_command = [
                                        command_str,
                                        str(files_to_process),
                                        '-f']
                    if paragraph:
                        full_command.append('-p')
                    Popen(full_command, creationflags=cmd_creationflags)
                    #print(f'The command to segment is: {full_command}')
                    #Popen(full_command)
        wf_segmented_files = _add_wf_files_to_group(path_files, self.ap_version)
        return wf_segmented_files
