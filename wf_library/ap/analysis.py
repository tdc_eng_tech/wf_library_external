__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from pathlib import Path


class Analysis:
    """
    Analyse the files with a TM or without
    """
    def __init__(self, ap_version: str, ap_path: str, debug: bool=False):
        """
        Initialize the object Analysis with the AP and Ap version
        """
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.tm = []
        self._debug = debug
    
    @property
    def debug(self):
        return self._debug
    
    @debug.setter
    def debug(self, value: bool):
        if isinstance(value, bool):
            self._debug = value
        else:
            raise TypeError(f'Wrong type,  expected <bool> and got {type(value)}')
    
    def add_tm(self, src_lang: str, tgt_lang: str, tm_string: str) -> None:
        """
        Add a TM to the object
        """
        tm = {}
        tm['source'] = src_lang
        tm['target'] = tgt_lang
        tm['string'] = tm_string
        self.tm.append(tm)
