__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from pathlib import Path
import platform
from subprocess import run, Popen
from .const import _no_console
from .commands import _add_wf_files_to_group


class PseudoTranslator:
    """
    This class controls the pseudotranslation of the files.
    """
    def __init__(self, config: str=None,
                 ap_version: str='txlf',
                 ap_path: str=r'c:\Program Files (x86)\Analysis Package\AP4',
                 debug: bool=False):
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.config_path = None
        self.config_str = None
        if config:
            if Path(config).exists() and Path(config).is_file():
                self.config_path = Path(config)
                self.config_str = str(self.config_path)
        self.ap_version = ap_version
        self.os = platform.system()
        self._debug = debug
    
    @property
    def debug(self):
        return self._debug
    
    @debug.setter
    def debug(self, value: bool):
        if isinstance(value, bool):
            self._debug = value
        else:
            raise TypeError(f'Wrong type,  expected <bool> and got {type(value)}')
    
    def pseudotranslate(self, files_path_str: str, target_lang: str='de-DE', threaded: bool=False, capture_output: bool=True):
        """
        Pseudotranslate the files in the path.
        
        Arguments:
            
        """
        if self.debug:
            cmd_creationflags = 0
        else:
            cmd_creationflags = _no_console
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, f'pseudotranslate{self.ap_version}.cmd')
        else:
            command = Path(self.ap_bin_str, f'pseudotranslate{self.ap_version}.ksh')
        if self.config_str:
            command_str = str(command)
        else:
            command_str = str(command)
        files_path = Path(files_path_str)
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            ap_exists = False
            return -1
        if ap_exists and Path(self.ap_bin_str).exists():
            if not threaded:
                files_to_process = files_path_str
                if self.config_str:
                    full_command = [command_str,
                                         '-c',
                                         self.config_str, 
                                         str(files_to_process)]
                else:
                    full_command = [command_str, 
                                         str(files_to_process)]
                if target_lang:
                    full_command.extend(['-p', target_lang])
                if self.ap_version == 'txlf':
                    full_command.append('-b')
                res = run(full_command, capture_output=capture_output, creationflags=cmd_creationflags)
                if capture_output:
                    res = res.stderr.decode('utf-8')
            if files_path.is_dir() and threaded:
                for file in files_path.glob('*.' + self.ap_version):
                    files_to_process = str(file)
                    if self.config_str:
                        full_command = [command_str, 
                                             '-c', 
                                             self.config_str, 
                                             str(file)]
                    else:
                        full_command = [command_str,
                                             str(file)]
                    if self.ap_version == 'txlf':
                        full_command.append('-b')
                    process = Popen(full_command, creationflags=cmd_creationflags)
                    res = process.communicate()[1]
        if capture_output:
            if 'No txlf files found' in res:
                return res
        wf_pseudotranslated_files = _add_wf_files_to_group(files_path, self.ap_version)
        return wf_pseudotranslated_files
