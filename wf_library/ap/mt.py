__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from pathlib import Path
from subprocess import run
import platform


def mt_translate(file: str, config_file: str, src_lang: str, tgt_lang: str, ap_path: str) -> object:
    """
    Get the translation of a file.
    Experimental
    """
    system = platform.system()
    if system == 'Windows':
        command = str(Path(ap_path, 'bin/mtExtract.cmd'))
    else:
        command = str(Path(ap_path, 'bin/mtExtract.ksh'))
    full_command = [command, file, '-c', config_file, '-l', src_lang, '-p', tgt_lang]
    print('Path to file exists:', Path(file).exists())
    res = run(full_command, capture_output=True, shell=False)
    print(res, flush=True)
    return res
