__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from pathlib import Path
from subprocess import Popen
import subprocess
import os


from .const import _no_console
from ..xml_wf import Wf, WfGroup


def run_command(command, threaded, use_shell, show_console, path_str=''):
    """
    Run an external command through subprocess.
    """
    path = Path(path_str)
    if path.exists() and path.is_dir():
        if threaded:
            sh_command = Popen(command,  shell=use_shell, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
        else:
            sh_command = Popen(command,  shell=use_shell, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
            sh_command.wait()
    else:
        if threaded:
            sh_command = Popen(command,  shell=use_shell, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
        else:
            sh_command = Popen(command,  shell=use_shell, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
            sh_command.wait()
    return sh_command


def subprocess_args():
    """
    """
    if hasattr(subprocess, 'STARTUPINFO'):
                        startupinfo = subprocess.STARTUPINFO()
                        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                        env = os.environ
    else:
        startupinfo = None
        env = None
    ret = {'stdout': subprocess.PIPE}
    ret.update({'stdin': subprocess.PIPE, 
                      'stderr': subprocess.PIPE, 
                      'startupinfo': startupinfo, 
                      'env': env })

def _add_wf_files_to_group(files_path, ap_version):
    """
    Add the generated wf objects to a WfGroup.Com
    
    Arguments:
        files_path (str): Path to the files to add to the group
        ap_version (str): Version of AP to be used (txlf/txml).
    """
    ap_version = ap_version.replace('.', '')
    wf_group = WfGroup()
    if Path(files_path).is_dir():
        for file in files_path.glob('**/*.{}'.format(ap_version)):
            file_to_process = str(file)
            wf_file = Wf(file_to_process)
            wf_group.add(wf_file)
    if Path(files_path).is_file():
        if str(Path(files_path).suffix).replace('.', '') == ap_version:
            wf_file = Wf(str(files_path))
            wf_group.add(wf_file)
        else:
            parent_folder = Path(Path(files_path).parent)

            extension = '*.{}'.format(ap_version)
            for file in parent_folder.glob(extension):
                file_to_process = str(file)
                print(f'adding the {file_to_process} to the group')
                if file.stem == Path(files_path).name:
                    wf_file = Wf(file_to_process)
                    wf_group.add(wf_file)
    return wf_group
