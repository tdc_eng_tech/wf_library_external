__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


import re


class Sheet:
    """
    Definition of the sheet of an ExcelConfig
    """
    def __init__(self):
        """
        Construct and initialize a Sheet object.
        
        Properties:
            name (str): Name of the sheet. Incompatible with number_sheet.
            number_sheet (int): Number of the sheet in the Excel. Incompatible with name.
            start_row (int): First row to be parsed. 0-Int based number.
            end_row (int): Last row to be parsed. 0-Int based number.
        """
        self._sheet_name = None
        self._number_sheet = None # Number of the sheet from 0 to n-1
        self._start_row = 0
        self._end_row = None
        self._source_column_range = ''
        self._source_column = None
        self._target_column = None
        self._maxlen_column = None
        self._attributes_columns = dict() # From PD15, name of attribute and column
        self._attributes_row = dict()
        self._columns = []
    
    @property
    def name(self):
        """
        Name of the worksheet. It is not compatible with number_sheet property.
        Only name or number_sheet can be set up.
        
        Example:
            sheet.name = 'Test' # Set the sheet name as 'Test'
            sheet.name return the name of the sheet.
            
        """
        if self._sheet_name:
            return self._sheet_name
    
    @name.setter
    def name(self, value):
        
        pat = re.compile('\w')
        try:
            pat.search(value)
        except TypeError:
            return -1
        if not self._number_sheet:
            self._sheet_name = value
        else:
            self._number_sheet = None
            self._sheet_name = value
    
    @property
    def number_sheet(self):
        """
        It is used to set the number of sheet to be added to the config.
        it is not compatible with name. Only one of them can be used.
        """
        if self._number_sheet:
            return self._number_sheet
    
    @number_sheet.setter
    def number_sheet(self, value):
        pat = re.compile('^\d+?$')
        if pat.search(str(value)):
            self._number_sheet = value
        if not self._sheet_name:
            self._sheet_name  = ''
    
    @property
    def source_column_range(self):
        """
        Return the range of source columns.
        """
        if self._source_column_range:
            return self._source_column_range
        else:
            return None
    
    def set_source_column_range(self, value):
        """
        Set the source column range.
        
        Arguments:
            value (str): Range of columns in scope.
        """
        # Check '^([0-9]{1,5}(-[0-9]{1,5})?)(;([0-9]{1,5}-[0-9]{1,5}|[0-9]{1,5}))?$'. 
        # Check if the value is a valid Excel range.
        # The value should be something like "0-1;5;7-9"
        # The Regex returnd None if there is anything else.
        
        pat_syntax_range = re.compile('^([0-9]{1,5}(-[0-9]{1,5})*)'
                                     '(;([0-9]{1,5}-[0-9]{1,5}|[0-9]{1,5}))*$')
        if pat_syntax_range.match(value) and not self.columns:
            self._source_column_range = value
        else:
            return -1
        return 0
    
    @property
    def start_row(self):
        """
        Indicate the initial row to be parsed.
        """
        if self._start_row:
            return str(self._start_row)
    
    @start_row.setter
    def start_row(self, value):
        if re.match('[0-9]{1,9}', str(value)) and int(value)>=0:
            self._start_row = str(value)
    
    @property
    def end_row(self):
        """
        Indicate the last row to be parsed
        """
        if self._end_row:
            return str(self._end_row)
    
    @end_row.setter
    def end_row(self, value):
        """
        """
        if re.match('[0-9]{1,9}', str(value)) and int(value)>=0:
            self._end_row = str(value)
    
    @property
    def columns(self):
        """
        It returns the columns that are in scope
        """
        return self._columns

    @property
    def attributes_columns(self):
        """
        Only read property to get the attributes columns
        """
        return self._attributes_columns

    def set_attribute_column(self, attribute_name: str, column: int) -> int:
        """
        Sets the column and attribute name containing the different attributes.
        
        Arguments:
            attribute_name (str): The name of the attribute to be added.
            column (int): The column value in base 0 values.
        
        Return:
            0: If is successful
            1: If one of the values is not valid
        """
        if isinstance(attribute_name, str) and isinstance(column, int):
            self._attributes_columns[attribute_name] = column
            return 0
        else:
            return 1
            
    
    def add_column(self, source, target=None, maxlen=None, attributes: dict={}):
        """
        Add a column to the columns list
        Arguments:
            source (int): Integer value of the source column. Starting 0-number.
            target  (int): Integer value of the traget column. 0-number format.
            maxlen (int): Integer value of the length restriction column. 0-number format
            attributes (dict): Contains the column in 0-number integer format and the name for these attributes.
        
        Example:
            self.add_column(3, 4, 2, {'Key': 0})
        """
        if not self.source_column_range:
            column =dict()
            if int(source) >= 0:
                column['source'] = int(source)
                
            if int(target) >= 0:
                column['target'] = int(target)
            elif not target:
                column['target'] = column['source']
            if maxlen and int(maxlen) > -1:
                column['maxlen'] = int(maxlen)
            else:
                column['maxlen'] = None
            if attributes:
                column['attributes'] = dict()
                for attr, col in attributes.items():
                    column['attributes'][attr] = col
            self._columns.append(column)

        else:
            return self.source_column_range 
        return 0
    
    @property
    def attributes_row(self):
        """
        Read only property. It returns all the rows that contain contextual information in the Config file.
        To set up a row, use the method set_attribute_row.
        """
        return self._attributes_row
    
    def set_attributes_row(self, attribute_name: int, row: int) -> int:
        """
        Set up an attribute row with its name and row position. It is only supported from the AP in PD 6.5 (June 2021)
        
        Arguments:
            attribute_name (str): The name that will receive this attribute
            row (int): Position of the row.
        """
        if isinstance(attribute_name,  str) and isinstance(row, int):
            self._attributes_row[attribute_name] = row
            return 0
        else:
            return 1
