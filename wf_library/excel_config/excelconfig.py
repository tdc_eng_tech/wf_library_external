__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


import re
from lxml import etree as et
import warnings

from .sheet import Sheet


class ExcelConfig:
    """
    Create an Excel Config.
    """
    def __init__(self, ex_conf_path=''):
        """
        Initialize the class
        """
        self._ex_conf_path = ex_conf_path
        self._regexp = ''
        self.trans_sheet_names = 'false'
        self.formula_cells = 'false'
        self.hidden_cells = 'false'
        self.hidden_sheets = 'false'
        self.cell_comments = 'false'
        self.data_validation_comments = 'false'
        self.drop_down_list = 'true'
        self.number_extraction_on_text = 'false'
        self.column_order = 'true'
        self.break_strategy = 'break'
        self.shapes = 'true' # New from Filters 5.5
        self.charts = 'true' # New from Filters 5.7
        self._sheets = []
#        if self._ex_conf_path and Path(self._ex_conf_path).exists():
#            self.__parse_config()
    
    @property
    def regexp(self):
        """
        Adds a regexp to filter the contents out of scope for the converter
        convert these contents as internal tags.
        """
        return self._regexp
    
    @regexp.setter
    def regexp(self, value):
        if isinstance(value, str):
            try:
                re.compile(value)
            except re.error:
                return -1
            self._regexp = value
    
    @property
    def sheets(self):
        """
        Return all the sheets contained in the config
        """
        return self._sheets
    
    def add_sheet(self, sheet: Sheet):
        """
        Adds a sheet to the Excel Config
        """
        if isinstance(sheet, Sheet):
            self._sheets.append(sheet)
            return sheet
        else:
            return -1

    def __parse_config(self):
        """
        Read an existing config and convert it to an object.
        It is an experimental function, use with caution.
        """
        if self._ex_conf_path:
            tree = et.parse(str(self._ex_conf_path))
            for elem in tree.iter():
                if elem.tag == 'regexp':
                    if elem.text:
                        self.regexp = elem.text
                elif elem.tag == 'translate_sheet_names':
                    self.trans_sheet_names = elem.text
                elif elem.tag == 'translate_formula_cells':
                    self.formula_cells = elem.text
                elif elem.tag == 'translate_hidden_cells':
                    self.hidden_cells = elem.text
                elif elem.tag == 'translate_hidden_sheets':
                    self.hidden_sheets = elem.text
                elif elem.tag == 'translate_cell_comments':
                    self.cell_comments = elem.text
                elif elem.tag == 'translate_data_validation_comments':
                    self.data_validation_comments = elem.text
                elif elem.tag == 'translate_drop_down_lists':
                    self.drop_down_list = elem.text
                elif elem.tag == 'number_extraction_on_text_cells':
                    self.number_extraction_on_text = elem.text
                elif elem.tag == 'extract_cell_order_by_column':
                    self.column_order = elem.text
                elif elem.tag == 'sheets':
                    self.__parse_sheets(elem)

    def __parse_sheets(self, sheets: et.Element):
        """
        Parse the sheets elements
        
        Arguments:
            sheets: Sheet elements extracted from the main parser
        """
        for sh in sheets.findall('sheet'):
            sheet = Sheet()
            for elem in sh.iter():
                sheet.name = elem.text if elem.tag == 'name' and elem.text else None
                sheet.number_sheet = elem.text if elem.tag == 'number' and elem.text else None
                if elem.tag == 'columns':
                    sheet = self.__parse_columns(elem, sheet)
            self.add_sheet(sheet)

    def __parse_columns(self, columns: et.Element, sheet: Sheet):
        """
        Parse the columns section of an existing ExcelConfig.
        """
        if columns.find('column') is not None:
            for col in columns.findall('column'):
                attributes = {}
                for elem in col.iter():
                    source = elem.text if elem.tag == 'sourceColumn' and elem.text else None
                    target = elem.text if elem.tag == 'targetColumn' and elem.text else None
                    maxlen = elem.text if elem.tag == 'lengthrestricctionColumn' and elem.text else None
                    if elem.tag == 'customAttributeColumn':
                        attr_name = elem.get('name')
                        attr_column = str(elem.text)
                        attributes[attr_name] = attr_column
                sheet.add_column(source, target, maxlen, attributes )
        if columns.find('sourceColumnRange') is not None:
            sheet.set_source_column_range(columns.find('sourceColumnRange').text)
        return sheet

    def write(self, save_to: str=''):
        """
        Save the configuration into an XML excelConfig file.
        Arguments:
            save_to (str): Path and name of the config where be saved.
        """
        # configuration as Root element of the XML.
        configuration = et.Element('configuration')
        # Generate Element Tree of Configuration
        config = et.ElementTree(configuration)
        if self.regexp:
            regexp = et.SubElement(configuration, 'regexp')
            regexp.text = self.regexp
        translate_sheet_names = et.SubElement(configuration, 'translate_sheet_names')
        translate_sheet_names.text = self.trans_sheet_names
        formula_cells = et.SubElement(configuration, 'translate_formula_cells')
        formula_cells.text = self.formula_cells
        hidden_cells = et.SubElement(configuration, 'translate_hidden_cells')
        hidden_cells.text = self.hidden_cells
        hidden_sheets = et.SubElement(configuration, 'translate_hidden_sheets')
        hidden_sheets.text = self.hidden_sheets
        cell_comments = et.SubElement(configuration, 'translate_cell_comments')
        cell_comments.text = self.cell_comments
        data_validation_comments = et.SubElement(configuration, 'translate_data_validation_comments')
        data_validation_comments.text = self.data_validation_comments
        drop_down_list = et.SubElement(configuration, 'translate_drop_down_lists')
        drop_down_list.text = self.drop_down_list
        number_extraction_on_text = et.SubElement(configuration, 'number_extraction_on_text_cells')
        number_extraction_on_text.text = self.number_extraction_on_text
        shapes_content = et.SubElement(configuration, 'extract_shapes_content')
        shapes_content.text = self.shapes
        charts_content = et.SubElement(configuration, 'extract_charts_content')
        charts_content.text  = self.charts
        extract_soft_return_strategy = et.SubElement(configuration, 'extract_soft_return_strategy')
        extract_soft_return_strategy.text = self.break_strategy
        column_order = et.SubElement(configuration, 'extract_cell_order_by_column')
        column_order.text = self.column_order       
        if self.sheets:
            sheets = et.SubElement(configuration, 'sheets')
            sheets.set('{http://www.w3.org/XML/1998/namespace}space', 'preserve')
            
            for sheet in self.sheets:
                xml_sheet = et.SubElement(sheets, 'sheet')
                if sheet.name and not sheet.number_sheet:
                    sheet_name = et.SubElement(xml_sheet, 'name')
                    sheet_name.text = sheet.name
                elif sheet.number_sheet:
                    number_sheet = et.SubElement(xml_sheet, 'number')
                    number_sheet.text = str(sheet.number_sheet)
                if sheet.start_row:
                    start_row = et.SubElement(xml_sheet, 'rowFromNumber')
                    start_row.text = str(sheet.start_row)
                if sheet.end_row:
                    end_row = et.SubElement(xml_sheet, 'rowEndNumber')
                    end_row.text = str(sheet.end_row)
                # Write the contextual row information From PD 6.5 (Filters 6.2).
                if sheet.attributes_row:
                    for attr, row in sheet.attributes_row.items():
                        print(attr,  row)
                        attribute_row = et.SubElement(xml_sheet, 'customAttributeRow')
                        attribute_row.set('name', attr)
                        attribute_row.text = str(row)
                if sheet.columns and not sheet.source_column_range:
                    columns = et.SubElement(xml_sheet, 'columns')
                    for column in sheet.columns:

                        if column['source'] >= 0:

                            column_xml = et.SubElement(columns, 'column')
                            source = et.SubElement(column_xml, 'sourceColumn')                            
                            source_str = str(column['source'])
                            source.text = source_str
                            if column['target'] >= 0:
                                target = et.SubElement(column_xml, 'targetColumn')
                                target.text = str(column['target'])
                            if column['maxlen']:
                                maxlen = et.SubElement(column_xml, 'lengthRestrictionColumn')
                                maxlen.text = str(column['maxlen'])
                            if column.get('attributes'):
                                for attr, col in column['attributes'].items():
                                    print(attr, col)
                                    attribute_column = et.SubElement(column_xml, 'customAttributeColumn')
                                    attribute_column.set('name', attr)
                                    attribute_column.text = col
                if not sheet.columns and sheet.source_column_range:
                    columns = et.SubElement(xml_sheet, 'columns')
                    source_column_range = et.SubElement(columns, 'sourceColumnRange')
                    source_column_range.text = str(sheet.source_column_range)
        if save_to:
            # Save the XML.
            print(save_to)
            config.write(str(save_to), encoding='utf-8', pretty_print=True, 
                 xml_declaration=True )
        else:
            if self.ex_conf_path:
                # Save the XML.
                print(self.ex_conf_path)
                config.write(str(self.ex_conf_path), encoding='utf-8', pretty_print=True, 
                 xml_declaration=True )
    
    def write_config(self, save_to: str):
        warnings.warn('Deprecated metod, use instead "write"', DeprecationWarning)
        self.write(save_to)

