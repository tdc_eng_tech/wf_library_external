__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


from datetime import datetime
import re


def validate_timestamp(value: str) -> bool:
    """
    Validate timestamp if is valid according to WF parameters
    Args:
        value (str):
            DESCRIPTION
    
    Returns:
        bool: DESCRIPTION
    """
    pat = re.compile(r'(?P<year>[0-9]{4})(?P<month>[0-9]{2})(?P<day>[0-9]{2})'
                            r'T(?P<hour>[0-9]{2})[0-9]{2}[0-9]{2}Z')
    res = pat.search(value)
    if res:
        datefmt = '%Y%m%dT%H%M%SZ'
        try:
            datetime.strptime(value, datefmt)
            return True
        except ValueError:
            return False
    else:
        return False
    
