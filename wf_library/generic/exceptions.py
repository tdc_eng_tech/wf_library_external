__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'


class Error(Exception):
    def __init__(self, message):
        pass


class WfTypeError(Error):
    """
    """
    def __init__(self, expression, message='The type of the file is not valid. It mustbe .txlf or .txml.'):
        self.expression = expression
        self.message = message

    def __str__(self):
        return f'{self.expression}. {self.message}'


class APRulesError(Error):
    """
    """
    def __init__(self, expression, message='The indicated rules seems that does not exist'):
        self.expression = expression
        self.message = message
    
    def __str__(self):
        return f'{self.expression}. {self.message}'
    
